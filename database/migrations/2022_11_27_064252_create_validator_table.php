<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValidatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validator', function (Blueprint $table) {
            $table->id('id');
            $table->softDeletes();
            $table->date('tanggal_order');
            $table->string('id_order');
            $table->string('pengirim');
            $table->string('penerima');
            $table->integer('no_penerima');
            $table->integer('no_pengirim');
            $table->string('layanan');
            $table->integer('berat');
            $table->integer('kodepos_asal');
            $table->integer('kodepos_tujuan');
            $table->integer('biaya');
            $table->integer('voucher');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validator');
    }
}
