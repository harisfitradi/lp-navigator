<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
	<link rel="stylesheet" href="/bootstrap-fe/css/login-style.css">
	<body>
		<div class="login-content">
		<img class="logo" src="bootstrap-fe/images/posaja-validator-logo.png" alt="logo">
			<div class="container-login" >
				@if (session()->has('error-login'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					{{ session('error-login') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				@endif
				@if (session()->has('success-logout'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					{{ session('success-logout') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				@endif
				<main class="form-signin">
					<form action="{{ route ('authenticate') }}" method="POST">
						@csrf
						{{-- {{csrf_field()}} --}}
						<h1 class="h3 mb-3 fw-normal">Selamat Datang</h1>
						<h2 class="h6 mb-3 fw-normal">Silahkan Masuk</h2>
						<div class="form-floating">
							<label for="floatingInput">Username</label>
							<input type="string" class="form-control" id="floatingInput" placeholder="Username/ID" name="idpetugas" required>
						</div>
						<div class="form-floating">
							<label for="floatingPassword">Password</label>
							<input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password" required>
						</div>
						<div class="checkbox mb-3">
							<label>
								<input type="checkbox" value="remember-me"> Ingat Saya
							</label>
						</div>
						<button class="w-100 btn btn-lg btn-primary" type="submit"><b>Login</b></button>
					</form>
				</main>
			</div>
			<p class="copyright">© Copyright 2022 <b>POS INDONESIA</b></p>
		</div>
	</body>
</html>

