<footer class="footer">
  <div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-md-9 text-center">
        <div class="footer-site-logo mb-4">
            <img src="/bootstrap-fe/images/posindonesia-logo.png" alt="Logo Pos Indonesia" class="img-fluid" style="width:10%">
        </div>
        <div class="copyright">
          <p><small>© Copyright 2022 <b>POS INDONESIA</b></small></p>
        </div>
      </div>
    </div>
  </div>
</footer>
