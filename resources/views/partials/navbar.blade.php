<!-- <style>
  body.show-sidebar{
    overflow: hidden;
    position: fixed;
    width: 100%; 
  }
</style> -->
<aside class="sidebar">
    <div class="toggle">
      <a href="#" class="burger js-menu-toggle" data-toggle="collapse" data-target="#main-navbar">
            <span></span>
          </a>
    </div>
    <div class="side-inner">
      <div class="profile">
        <div class="account">
          <img src="/bootstrap-fe/images/profile-pic.jpg" alt="Image" class="img-fluid"> 
            <div class="user">
              <h3 class="name">{{ session()->get('username') }}</h3>
                <span class="country">{{ session()->get('jabatan') }}</span>
            </div> 
        </div>
        <a href="/logout"><button type="button" class="button"><i class="fa-solid fa-right-from-bracket mr-3"></i>Logout</i></button></a>
      </div>
      

      
      <div class="nav-menu">
        <label style="font-size:12px; font-weight:lighter; color:gray; margin-bottom:24px">MENU</label>
        <ul>
        <li class="{{ request()->is('import*') ? 'active' : '' }}"><a href="/import"><i class="fa-regular fa-boxes-stacked mr-3"></i>Serah Terima</a></li>
        <li class="{{ request()->is('validasi*') ? 'active' : '' }}"><a href="/validasi"><i class="fa-solid fa-list-check mr-3"></i>Validasi</a></li>
        <li class="{{ request()->is('report*') ? 'active' : '' }}"><a href="/report"><i class="fa-solid fa-chart-pie mr-3"></i>Report</a></li>
        <li class="{{ request()->is('resi*') ? 'active' : '' }}"><a href="/resi"><i class="fa-regular fa-print mr-3"></i>Cetak Resi</a></li>

          {{-- <li><a href="#"><span class="icon-location-arrow mr-3"></span>Menu 4</a></li>
          <li><a href="#"><span class="icon-pie-chart mr-3"></span>Menu 5</a></li> 
          <li><a href="#"><span class="icon-sign-out mr-3"></span>Keluar</a></li> --}}
        </ul>
      </div>
    </div>
    
  </aside>