@extends('layouts.main')
@section('container')

@push('import-style')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="/bootstrap-fe/datatables/dataTables.bootstrap4.min.css">
<link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/datetime/1.2.0/css/dataTables.dateTime.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.2.0/js/dataTables.dateTime.min.js"></script>
<script src="https://cdn.datatables.net/searchpanes/2.1.0/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"></script>
@endpush

<style>

.container-import {
  display: block;
  position: relative;
  padding-left: 35px;
  margin: 16px;
  cursor: pointer;
  font-size: 16px;
  color: #EC8057;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container-import input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container-import:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container-import input:checked ~ .checkmark {
  background-color: #EC8057;
  color: #EC8057 !important;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container-import input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container-import .checkmark:after {
 	top: 8px;
	left: 8px;
	width: 9px;
	height: 9px;
	border-radius: 50%;
	background: white;
}

/* import */

.import-barang{
    width: 100%;
    margin: auto;
    position: relative;
    height: fit-content;
    background: #f9f9f9;
    border-radius: 8px;
    border: 1px solid #eee;
    padding: 16px;
}
.btn-import{
    display: flex;
    align-items: center;
    justify-content: center;
}
.import-btn{
    margin: 16px;
    float: right;
    width: 25%;
    padding: 8px 16px;
    background: #EC8057;
    border: 1px solid #EC8057 !important;
    color: #fff !important; 
    border-radius: 8px;
    cursor: pointer;
}
.import-btn.disabled{
    margin: 16px;
    width: 25%;
    padding: 8px 16px;
    background: #eee;
    border: 1px solid #eee !important;
    color: lightgray !important; 
    border-radius: 8px;
}
.import-btn.disabled:hover{
    background: #eee;
    border: 1px solid #eee !important;
    color: lightgray !important;
}
.import-btn:hover{
    background: #EC8057;
    border: 1px solid #EC8057 !important;
    color: #fff !important;
}
.show-items{
    width: 100%;
    margin: auto;
    position: relative;
    height: fit-content;
    background: #fff;
    border-radius: 8px;
    border: 1px solid #eee;
    padding: 16px;
}

.show-form{
  display: none;
  background: #fff;
  padding: 24px;
  border-radius: 12px;
  border: 1px solid #eee;
  margin-top: 24px;
}

/* Style the input field */
#myInput {
    padding: 20px;
    margin-top: -6px;
    border: 0;
    border-radius: 0;
    background: #f1f1f1;
  }

</style>

@if (session()->has('success-login'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{ session('success-login') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row justify-content-center">
  <div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Penerimaan Barang</h1>
    </div>

    <!-- button import kiriman -->
    <div class="import-barang">
      <div class="btn-import">
        <h6>Pilih Metode Import: </h6>
        <label for="import" class="container-import">Import Items 
        <input type="radio" onclick="showData()" id="import" name="import_mtd" value="Import Items">
        <span class="checkmark"></span><i class="fa-regular fa-cloud-arrow-up mr-3"></i>
        </label>
        <label for="scan" class="container-import">Scan QR 
        <input type="radio" onclick="showForm()" id="scan" name="import_mtd" value="Scan QR">
        <span class="checkmark"></span><i class="fa-solid fa-qrcode mr-3"></i>
        </label>
      </div>

    {{-- show form scan qr --}}
    <div class="show-form" id="showFormScan">
      <div class="row g-3">
        <div class="col-12">
          <label for="inputAddress" class="form-label">Nomor QR Code</label>
          <input type="text" onkeyup="stoppedTyping()" class="form-control" id="qrCode" value="QOB55942986091302" placeholder="Nomor QR Code akan muncul disini...">
          <button class="btn btn-info mt-2" type="button" id="cariId">Cari</button>
        </div>
      </div>
      <div class="row">
        <div class="table-responsive" id="tabel2">
          <table class="table table-bordered" id="table_import_2" width="100%" cellspacing="0">
              
          </table>
        </div>
      </div>
      {{-- <div class="row">
        <div class="col-12">
          <button class="import-btn" type="submit" name="submit" id="tombol_terima_2">Terima Barang</button>
        </div>
      </div> --}}
    </div>

    {{-- show form cari pickuper --}}
    <div class="show-items" id="showBarang" style="display:none; margin-top:24px;">
      <div class="card-body">
        <div class="row mb-8 col-12">
            <div class="form-group col-lg-3">
                <label for="tglPickup">Pilih Tanggal Pickup</label>
                <input type="date" id="tglPickup" name="tglPickup" class="form-control" placeholder="Pilih Tanggal Pickup" value="{{ date('2022-06-15') }}">
            </div>
            <div class="form-group col-lg-3">
              <label for="tglPickup">Pilih Pickuper</label>
              <select class="form-select form-control" aria-label="Oranger" id="paramOranger" name="paramOranger">
                <option value="820000001">Martin Pickuper</option>
                <option value="820000004">Martindes Pickuper</option>
                <option value="590000023" selected>DEDI JUNAEDI</option>
              </select>
            </div>
            <div class="form-group col-lg-3">
              <button class="btn btn-info ml-1" type="submit" id="cariBarang" style="margin-top:30px">Cari</button>
            </div>
        </div>
      </div>

      <div class="table-responsive" id="tabel1">
        <table class="table table-bordered" id="table_import_1" width="100%" cellspacing="0">
            
        </table>
      </div>
      {{-- <div class="row">
        <div class="col-12">
          <button class="import-btn" type="submit" name="submit" id="tombol_terima_1">Terima Barang</button>
        </div> --}}
      </div>
    </div>
  </div>
</div>

@push('validasi-script')
<script>
  $('#tombol_terima_1').on('click',function(e){
    // e.preventDefault();
    let id_barang;
    $('#list-monitoring-1 tbody tr').each(function() {
      id_barang = $(this).find(".qob").html();
      console.log(id_barang);    
    });

    $.ajax({
      url: "{{ route('terima.barang.pickuper') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : id_barang,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  console.log(data);
                  $('#tabel2').empty();
                  $('#tabel2').html(data.response);
                }else{
                  alert(data.ket);
                }
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
    });
  });

  $('#tombol_terima_2').on('click',function(e){
    // e.preventDefault();

    let id_barang;
    $('#list-monitoring-2 tbody tr').each(function() {
      id_barang = $(this).find(".qob").html();
      console.log(id_barang);    
    });

    $.ajax({
      url: "{{ route('terima.barang.qr') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : nomorScanQr,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  console.log(data);
                  $('#tabel2').empty();
                  $('#tabel2').html(data.response);
                  
                }else{
                  alert(data.ket);
                }
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
    });
  });

  $('#cariId').on('click',function(e){
    e.preventDefault();
    const nomorScanQr = $('#qrCode').val();

    $.ajax({
      url: "{{ route('get.status.import') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : nomorScanQr,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  // console.log(data);
                  $('#tabel2').empty();
                  // $('#tabel2').html(data.response);
                  alert(data.response);
                  ajax_monitoring_qr(nomorScanQr);
                  
                }else{
                  alert(data.ket);
                }
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
    });
  });


  $('#cariBarang').on('click',function(e){
      e.preventDefault();

      const tglPickup = $('#tglPickup').val();
      const DdatePickup = new Date($('#tglPickup').val());
      const Oranger = $('#paramOranger').val();
      if((tglPickup == '') || (tglPickup == null)){
          alert('Periode tanggal awal wajib diisi!');
		  }else{
        $.ajax({
            url: "{{ route('get.status.import.pickuper') }}",
            type: "POST",
            dataType: "json", 
            data:{
                tanggal:tglPickup,
                oranger:Oranger,
                "_token": "{{ csrf_token() }}"
            },
            // beforeSend: function () {
            //     $("#loading").show();
            // },
            success: function (data) {
                if(data.status == 1){
                  $('#tabel1').empty();
                  // $('#tabel1').html(data.response);
                  alert(data.response);
                  ajax_monitoring_pickuper(tglPickup);
                }else{
                  alert(data.ket);
                }
            },
            // complete: function(){
            //   $('html, body').animate({
            //           scrollTop: $("#table_import_1").offset().top
            //       }, 1000);
            // },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
        }
  });

function showForm() {
    document.getElementById('showBarang').style.display = "none";
    document.getElementById('showFormScan').style.display = "block";
 }

function showData() {
    document.getElementById('showFormScan').style.display = "none";
    document.getElementById('showBarang').style.display = "block";
 }

function checkAll(bx) {

  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
};function isChecked(checkbox, test){
  if (document.getElementById('checker').checked){
    document.getElementById(test).disabled=false;
    document.getElementById(test).className="import-btn";
  }
  else{
    document.getElementById(test).disabled=true;
    document.getElementById(test).className="import-btn disabled";
  }
}

function stoppedTyping(){
        if(document.getElementById('qrCode').value != "") { 
            document.getElementById('tombol2').disabled = false;
            document.getElementById('tombol2').className="import-btn"; 
        } else { 
            document.getElementById('tombol2').disabled = true;
            document.getElementById('tombol2').className="import-btn disabled";
        }
    }


function ajax_monitoring_qr(id_Barang) {
    $.ajax({
      url: "{{ route('post.monitoring.qr') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : id_Barang,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  console.log(data);
                  $('#tabel2').empty();
                  $('#tabel2').html(data.table_monitoring);
                  $("#list-monitoring-2").DataTable( {
                              "bDestroy": true,
                              processing: true, //Feature control the processing indicator.
                              lengthChange: true,
                              pageLength: 10,
                              // scrollX: true,
                              buttons: [  {
                                              extend: "excelHtml5",
                                              title: "Monitoring Validator"
                                          },
                                          {
                                              extend: "pdfHtml5",
                                              title: "Monitoring Validator"
                                          } ],
                              // "dom" : '<"row"<"col-sm-4"B><"col-sm-4"i><"col-sm-4"f>>rt<"row"<"col-sm-4"l><"col-sm-4"><"col-sm-4"p>>',
                              "dom" : "Bifrtlip",
                              "language": {
                                  "search": "Cari:",
                                  "emptyTable":     "Tidak ada data!",
                                  "info":           "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                                  "infoEmpty":      "Menampilkan 0 - 0 dari 0 data",
                                  "infoFiltered":   "(terfilterisasi dari _MAX_ jumlah data)",
                                  "thousands":      ".",
                                  "lengthMenu":     "Tampilkan _MENU_ data",
                                  "loadingRecords": "Loading...",   
                                  "processing": "Sedang proses.."                      
                              }
                  } );
                }else{
                  alert(data.ket);
                }
              },
            complete: function(){
              $('html, body').animate({
                      scrollTop: $("#list-monitoring-2").offset().top
                  }, 1000);
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            },
          });
  }

function ajax_monitoring_pickuper(tgl) {
  $.ajax({
    url: "{{ route('post.monitoring.pickuper') }}",
    type: "POST",
    dataType: "json",
    data: {
      tanggal : tgl,
      "_token": "{{ csrf_token() }}"
    },
    success: function(data) {
      if(data.status == 1){
                console.log(data);
                $('#tabel1').empty();
                $('#tabel1').html(data.table_monitoring);
                $("#list-monitoring-1").DataTable( {
                            "bDestroy": true,
                            processing: true, //Feature control the processing indicator.
                            lengthChange: true,
                            pageLength: 10,
                            // scrollX: true,
                            buttons: [  {
                                            extend: "excelHtml5",
                                            title: "Monitoring Validator"
                                        },
                                        {
                                            extend: "pdfHtml5",
                                            title: "Monitoring Validator"
                                        } ],
                            // "dom" : '<"row"<"col-sm-4"B><"col-sm-4"i><"col-sm-4"f>>rt<"row"<"col-sm-4"l><"col-sm-4"><"col-sm-4"p>>',
                            "dom" : "Bifrtlip",
                            "language": {
                                "search": "Cari:",
                                "emptyTable":     "Tidak ada data!",
                                "info":           "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                                "infoEmpty":      "Menampilkan 0 - 0 dari 0 data",
                                "infoFiltered":   "(terfilterisasi dari _MAX_ jumlah data)",
                                "thousands":      ".",
                                "lengthMenu":     "Tampilkan _MENU_ data",
                                "loadingRecords": "Loading...",   
                                "processing": "Sedang proses.."                      
                            }
                } );
                const plant = document.querySelector('.detailButton');
                const fruitCount = plant.getAttribute('data-extid');
                console.log(fruitCount);
              }else{
                alert(data.ket);
              }
            },
          complete: function(){
            $('html, body').animate({
                    scrollTop: $("#list-monitoring-1").offset().top
                }, 1000);
          },
          error: function(xhr, status, error) {
              var err = eval("(" + xhr.responseText + ")");
              alert(err.Message);
          },
        });
}

$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

$(document).on('click', '#list-monitoring-1 .detailButton', function(){
    // e.preventDefault();
    let id_barang = $(this).data('extid');
    console.log(id_barang);
    
    $.ajax({
      url: "{{ route('terima.barang') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : id_barang,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  // console.log(data);
                  $('#tabel1').empty();
                  // $('#tabel1').html(data.response);
                  alert(data.response);
                  // ajax_monitoring_pickuper(tglPickup);
                }else{
                  alert(data.ket);
                }
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
    });
    $(document).ajaxStop(function(){
    window.location.reload();
    });
});

$(document).on('click', '#list-monitoring-2 .detailButton', function(){
    // e.preventDefault();
    let id_barang = $(this).data('extid');

    console.log(id_barang);

    $.ajax({
      url: "{{ route('terima.barang') }}",
      type: "POST",
      dataType: "json",
      data: {
        idBarang : id_barang,
        "_token": "{{ csrf_token() }}"
      },
      success: function(data) {
        if(data.status == 1){
                  // console.log(data);
                  $('#tabel2').empty();
                  // $('#tabel2').html(data.response);
                  alert(data.response);
                  // ajax_monitoring_qr(id_barang);
                }else{
                  alert(data.ket);
                }
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
    });
    $(document).ajaxStop(function(){
    window.location.reload();
    });
});

</script>



@endpush
@endsection