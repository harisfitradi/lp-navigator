<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- 
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    --}}
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap-fe/fonts/icomoon/style.css">
    <link rel="stylesheet" href="/bootstrap-fe/css/owl.carousel.min.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css"> -->
    <script src="/bootstrap-fe/js/jquery-3.3.1.min.js"></script>  
   
    
    <!-- Icons -->
    <script src="https://kit.fontawesome.com/0c6d83ec7a.js" crossorigin="anonymous"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap-fe/css/bootstrap.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="/bootstrap-fe/css/style.css">
    <link rel="stylesheet" href="/bootstrap-fe/css/footer-style.css">
    
    <!-- notif -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <script type="javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script> -->
    @stack('validasi-style')
    <title>LP-Validator</title>


    
  </head>
  <body>
    @include('/partials.navbar')
    <main>
      <div class="site-section">
        <div class="container">
          @include('/partials.header')
          @yield('container')
          <div class="row mt-4">
            <div class="col-lg-12">
              @include('/partials.footer')
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- <script src="/bootstrap-fe/js/jquery-3.3.1.min.js"></script> -->
    
    <script src="/bootstrap-fe/js/popper.min.js"></script>
    <script src="/bootstrap-fe/js/bootstrap.min.js"></script>
    <script src="/bootstrap-fe/js/main.js"></script>
    <!-- <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    
    @stack('validasi-script')
    @stack('detail-validasi-script')
    <!-- notif -->
    <script>
    $(document).ready(function(){
            
            
            
        
            var down = false;
            
            $('#bell').click(function(e){
              
                var color = $(this).text();
                if(down){
                    
                    $('#box').css('height','0px');
                    $('#box').css('opacity','0');
                    $('#box').css('z-index','0');
                    $('#box').css('display','none');
                    down = false;
                }else{
                    
                    $('#box').css('height','auto');
                    $('#box').css('opacity','1');
                    $('#box').css('z-index','1');
                    $('#box').css('display','block');
                    down = true;
                    
                }
                
            });
                
                });
    </script>
  </body>
</html>