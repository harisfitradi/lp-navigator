@extends('layouts.main')

@section('container')
    

{{-- Template Detail Report --}}

<!-- Breadcrumb -->
<div class="row">
  <div class="col-lg-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color :#FFFFFF;">
        <li class="breadcrumb-item"><a href="/report">Report</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$idR}}</li>
      </ol>
    </nav>
  </div>
</div>
<!-- Table View -->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <!-- JUDUL REPORT -->
          <div class="col-lg-10 mt-2">
            <div class="ml-2">
              <h4 class="font-weight-bold">Detail Order</h4>
              <h6 class="font-weight-bold">{{$idR}}</h6>
              <h6>{{$tanggal}}</h6>
            </div>
          </div>
          <div class="col-lg-2 mt-2">
            <div class="ml-2">
              <h4 class="font-weight-bold">Status:</h4>
              <h4 class="text-success">{{$status}}</h4>
            </div>
          </div>
          <div class="col-lg-12">
            <hr>
            <table class="table table-borderless table-responsive">
              <tbody>
                <tr>
                  <th>Nama Pengirim</th>
                  <td>:</td>
                  <td>{{$namaKirim}}</td>
                </tr>
                <tr>
                  <th>Alamat Pengirim</th>
                  <td>:</td>
                  <td>{{$alamatKirim}}</td>
                </tr>
                <tr>
                  <th>Kode Pos Asal</th>
                  <td>:</td>
                  <td>{{$kodeAsal}}</td>
                </tr>
                <tr>
                  <th>No HP Pengirim</th>
                  <td>:</td>
                  <td>{{$hpPengirim}}</td>
                </tr>
                <tr>
                  <th>Nama Penerima</th>
                  <td>:</td>
                  <td>{{$namaTerima}}</td>
                </tr>
                <tr>
                  <th>Alamat Penerima</th>
                  <td>:</td>
                  <td>{{$alamatTerima}}</td>
                </tr>
                <tr>
                  <th>Kode Pos Tujuan</th>
                  <td>:</td>
                  <td>{{$kodeTujuan}}</td>
                </tr>
                <tr>
                  <th>No HP Penerima</th>
                  <td>:</td>
                  <td>{{$hpTerima}}</td>
                </tr>
                <tr>
                  <th>Jenis Layanan</th>
                  <td>:</td>
                  <td>{{$layanan1}}</td>
                </tr>
                <tr>
                  <th>Berat Paket (gr)</th>
                  <td>:</td>
                  <td>{{$berat}}</td>
                </tr>
                <tr>
                  <th>Nominal Barang (Rp)</th>
                  <td>:</td>
                  <td>{{$nominal}}</td>
                </tr>
                <tr>
                  <th>Tarif Terbayar (Rp)</th>
                  <td>:</td>
                  <td>{{$tarifBayar}}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Table Validasi -->
<div class="row mt-4">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <!-- JUDUL REPORT -->
          <div class="col-lg-10 mt-2">
            <div class="ml-2">
              <h4 class="font-weight-bold">Detail Validasi</h4>
            </div>
          </div>
          <div class="col-lg-12">
            <hr>
            <table class="table table-borderless table-responsive">
              <tbody>
                <tr>
                  <th>Jenis Layanan</th>
                  <td>:</td>
                  <td>{{$layanan1}}</td>
                </tr>
                <tr>
                  <th>Kode Pos Asal</th>
                  <td>:</td>
                  <td>{{$kodeAsal}}</td>
                </tr>
                <tr>
                  <th>Kode Pos Tujuan</th>
                  <td>:</td>
                  <td>{{$kodeTujuan}}</td>
                </tr>
                <tr>
                  <th>Berat Aktual Paket (gr)</th>
                  <td>:</td>
                  <td>{{$berat}}</td>
                </tr>
                <tr>
                  <th>Dimensi Paket (PxLxT) (cm)</th>
                  <td>:</td>
                  <td>{{$dimensi}}</td>
                </tr>
                <tr>
                  <th>Nominal Barang (Rp)</th>
                  <td>:</td>
                  <td>{{$nominal}}</td>
                </tr>
                <tr>
                  <th>Nominal Voucher (Rp)</th>
                  <td>:</td>
                  <td>{{$nominalVoucher}}</td>
                </tr>
                <tr>
                  <th>Tarif Aktual</th>
                  <td>:</td>
                  <td>30000</td>
                </tr>
                <tr>
                  <th>Selisih Bayar</th>
                  <td>:</td>
                  <td>0</td>
                </tr>
                <tr>
                  <th>Status Validasi</th>
                  <td>:</td>
                  <td><p class="text-success font-weight-bold">{{$status}}</p></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
<!-- Header dan Judul Order -->
<!-- <div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="detail-header-validasi">
          <div class="row">
            <div class="col-lg-6">
              <h5 style="font-weight: bold; font-size: 20px;">QOB0000001</h5>
              <h6 style="font-size: 12px;">22 November 2022</h6> <br>
            </div>
            <div class="col-lg-6" >
             <h6>Status : </h6>
             <h6 style=" font-family : Poppins; color :#46C06F; font-size :20px; font-weight:700;">Valid</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->


<!-- <div class="row formvalidasi mt-4">
  <div class="col-lg-12"> 
    <div class="card">
      <div class="card-body">
        <div class="col-lg-12 md-3 mt-2">
          <H5 style="font-weight: bold;">Detail Data</H5>
        </div>
        <div class="col-lg-12 mt-5">
          <form>
            <fieldset disabled> -->
              <!-- Nama Pengirim -->
              <!-- <div class="form-group">
                <label for="disabledTextInput">Nama Pengirim</label>
                <input type="text" id="disabledTextInput" class="form-control" placeholder="Eufamia Sholehah">
              </div> -->
              <!-- Alamat -->
              <!-- <div class="form-group">
                <label for="exampleFormControlTextarea1">Alamat Pengirim</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Kosan Rumah Kita, Kamar C2.13, Jl. Antapani, no. 50 (samping SMPN 49), RT01, RW05, Kel. Antapani Kulon, Kec. Antapani, Bandung, 40291"></textarea>
              </div> -->
              <!-- No HP Pengirim -->
              <!-- <div class="form-group">
                <label for="disabledTextInput">No. Handphone Pengirim</label>
                <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="08123456789">
              </div> -->
              <!-- Nama Penerima -->
              <!-- <div class="form-group">
                <label for="disabledTextInput">Nama Penerima</label>
                <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="Yohanes Bayu">
              </div> -->
              <!-- Alamat Penerima -->
              <!-- <div class="form-group">
                <label for="exampleFormControlTextarea1">Alamat Penerima</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Kosan Rumah Kita, Kamar C2.11, Jl. Antapani, no. 50 (samping SMPN 49), RT01, RW05, Kel. Antapani Kulon, Kec. Antapani, Bandung, 40291"></textarea>
              </div> -->
              <!-- No HP Penerima -->
              <!-- <div class="form-group">
                <label for="disabledTextInput">No. Handphone Penerima</label>
                <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="08987654321">
              </div> -->
              <!-- Layanan -->
              <!-- <div class="form-group">
                <label for="disabledTextInput">Layanan</label>
                <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="POS Instant">
              </div>
              <div class="row"> -->
                <!-- Berat Paket -->
                <!-- <div class="col-lg-6">
                  <div class="form-group">
                    <label for="disabledTextInput">Berat Paket (Gram)</label>
                    <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="3000">
                  </div>
                </div> -->
                <!-- Tarif Terbayar -->
                <!-- <div class="col-lg-6">
                  <div class="form-group">
                    <label for="disabledTextInput">Tarif terbayar</label>
                  <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="Rp.30000">
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div> -->
  <!-- Row Validasi -->
  <!-- <div class="col-lg-12 mt-4">
    <div class="card">
      <div class="card-body">
        <div class="col-lg-12">
          <div class="col-lg-12 md-3 mt-2">
            <H5 style="font-weight: bold;">Validasi</H5>
          </div>
          <div class="col-lg-12">
            <form>
              <fieldset disabled> -->
                <!-- Layanan -->
                <!-- <div class="form-group mt-5">
                  <label for="disabledTextInput">Layanan</label>
                  <div class="form-group">
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option>POS Instant</option>
                      <option>2</option>
                      <option>3</option>
                    </select>
                  </div>
                </div> -->
                <!-- Berat Aktual -->
                <!-- <div class="form-group">
                  <label for="disabledTextInput">Berat Paket (gram)</label>
                  <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="3000">
                </div> -->
                <!-- Volumetrik -->
                <!-- <div class="row">
                  <div class="col-lg-12">
                    <label for="disabledTextInput">Dimensi Paket (P x L x T)</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="10">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group"> 
                      <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="10">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">                            
                      <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="10">
                    </div>
                  </div>           
                </div> -->
                <!-- Tarif -->
                <!-- <div class="row">
                  <div class="col-lg-12">
                    <label for="disabledTextInput">Tarif</label>
                    <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="30000">
                  </div>
                </div> -->
                <!-- Status Validasi -->
                <!-- <div class="row">
                  <div class="col-lg-12 mt-4">
                    <div class="alert alert-success d-flex justify-content-center" role="alert">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M8.6 11.8L6.425 9.625C6.24167 9.44167 6.01267 9.35433 5.738 9.363C5.46267 9.371 5.23333 9.46667 5.05 9.65C4.86667 9.83333 4.775 10.0667 4.775 10.35C4.775 10.6333 4.86667 10.8667 5.05 11.05L7.9 13.9C8.08333 14.0833 8.31667 14.175 8.6 14.175C8.88333 14.175 9.11667 14.0833 9.3 13.9L14.975 8.225C15.1583 8.04167 15.246 7.81233 15.238 7.537C15.2293 7.26233 15.1333 7.03333 14.95 6.85C14.7667 6.66667 14.5333 6.575 14.25 6.575C13.9667 6.575 13.7333 6.66667 13.55 6.85L8.6 11.8ZM10 20C8.61667 20 7.31667 19.7373 6.1 19.212C4.88333 18.6873 3.825 17.975 2.925 17.075C2.025 16.175 1.31267 15.1167 0.788 13.9C0.262667 12.6833 0 11.3833 0 10C0 8.61667 0.262667 7.31667 0.788 6.1C1.31267 4.88333 2.025 3.825 2.925 2.925C3.825 2.025 4.88333 1.31233 6.1 0.787C7.31667 0.262333 8.61667 0 10 0C11.3833 0 12.6833 0.262333 13.9 0.787C15.1167 1.31233 16.175 2.025 17.075 2.925C17.975 3.825 18.6873 4.88333 19.212 6.1C19.7373 7.31667 20 8.61667 20 10C20 11.3833 19.7373 12.6833 19.212 13.9C18.6873 15.1167 17.975 16.175 17.075 17.075C16.175 17.975 15.1167 18.6873 13.9 19.212C12.6833 19.7373 11.3833 20 10 20ZM10 18C12.2167 18 14.1043 17.221 15.663 15.663C17.221 14.1043 18 12.2167 18 10C18 7.78333 17.221 5.89567 15.663 4.337C14.1043 2.779 12.2167 2 10 2C7.78333 2 5.896 2.779 4.338 4.337C2.77933 5.89567 2 7.78333 2 10C2 12.2167 2.77933 14.1043 4.338 15.663C5.896 17.221 7.78333 18 10 18Z" fill="#46C06F"/>
                      </svg>
                      <a class="alert-link ml-2">Data Valid</a>
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div> -->
          <!-- Button Cek Tarif -->
          <!-- <div class="row">
            <div class="col-lg-12 mt-4">
              <button type="button" class="btn btn-primary btn-lg btn-block hide" style="background-color: #FF9839; border-color: #FF9839">Cek Tarif</button>
            </div>
          </div> -->
          
          <!-- {{-- Cek Tarif Hide --}} -->
          <!-- <div class=""> -->
            <!-- Kurang Bayar -->
            <!-- <div class="row" style="color: red;">
              <div class="col-lg-12 mt-4">
                <label for="disabledTextInput">Kurang Bayar senilai Rp 5000,</label>
              </div>
            </div> -->
            <!-- Total Tarif -->
            <!-- <div class="row">
              <div class="col-lg-12 mt-4">
                <label for="disabledTextInput">Total Tarif</label>
                <fieldset disabled>
                  <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="Rp.35000">
                </fieldset>
              </div>
            </div>
            <div class="row mt-4"> -->
              <!-- Button Batal -->
              <!-- <div class="col-lg-6">
                <button type="button" class="btn btn-outline-primary btn-lg btn-block">Batal</button>
              </div> -->
              <!-- Button Kabari Pengirim -->
              <!-- <div class="col-lg-6">
                <button type="button" class="btn btn-primary btn-lg btn-block" style="background-color: #25D366; border-color: #25D366"><i class="fa fa-whatsapp" aria-hidden="true"></i>
                Kabari Pengirim</button>
              </div> -->
            <!-- </div> -->
          <!-- </div> -->
          <!-- {{-- End Cek Tarif Hide --}} -->
        <!-- </div>
      </div>
    </div>
  </div>
</div> -->

<!-- {{-- End-Template Detail Report --}} -->

