@extends('layouts.main')
@section('container')
@push('validasi-style')
<!-- Custom styles for this page -->
<link href="/bootstrap-fe/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/datetime/1.2.0/css/dataTables.dateTime.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.2.0/js/dataTables.dateTime.min.js"></script>
<script src="https://cdn.datatables.net/searchpanes/2.1.0/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
@endpush

<div class="row justify-content-center">
  <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Report</h1>
        
        <!-- DataTables -->

        <div class="card shadow mb-4">
            {{-- <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data</h6>
            </div> --}}
            <div class="card-body">
              <div class="row mb-8 col-12">
                  <div class="form-group col-lg-3">
                      <label for="min">Pilih Tanggal Awal</label>
                      <input type="date" id="min" name="min" class="form-control" placeholder="Pilih Tanggal Awal" value="{{ date('Y-m-d',strtotime('2022-06-01')) }}">
                  </div>
                  <div class="form-group col-lg-3">
                      <label for="max">Pilih Tanggal Akhir</label>
                      <input type="date" id="max" name="max" class="form-control" placeholder="Pilih Tanggal Akhir" value="{{ date('Y-m-d') }}">
                  </div>
                  <div class="form-group col-lg-3">
                      <label for="statusFilter">Pilih Status</label>
                    <select class="form-select form-control" aria-label="Status" id="statusFilter" name="statusFilter">
                      <option value="">Semua Status</option>
                      <option value="1">Belum Validasi</option>
                      <option value="2">Sudah Validasi</option>
                      <option value="3">Batal Order</option>
                      <option value="4">Hitung Ulang Tarif</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-3">
                    <button class="btn btn-info ml-3" type="submit" id="buttonCari" style="margin-top:30px;">Cari</button>
                  </div>
              </div>
            </div>
            <br>
            <div class="card-body">
                <div class="table-responsive">
                      <table id="table_report" class="table table-bordered" width="100%" cellspacing="0">
                      </table>
                </div>
            </div>

  </div>
</div>

@push('validasi-script')
<!-- <script src="/bootstrap-fe/js/custom-datatables.js"></script> -->

<script type="text/javascript">

  $('#buttonCari').on('click',function(e){
      e.preventDefault();
      const dateMin = $('#min').val();
      const dateMax = $('#max').val();
      const DdateMin = new Date($('#min').val());
		  const DdateMax = new Date($('#max').val());
      const status = $('#statusFilter').val();
      if((dateMin == '') || (dateMin == null)){
          alert('Periode tanggal awal wajib diisi!');
      }else if((dateMax == '') || (dateMax == null)){
          alert('Periode tanggal akhir wajib diisi!');
      }else if(DdateMin>DdateMax){
          alert('Tanggal periode awal tidak boleh lebih dari periode akhir!');
		  }else{
        $.ajax({
            url: "{{ route('post.report') }}",
            type: "POST",
            dataType: "json", 
            data:{
                tglAwal:dateMin,
                tglAkhir:dateMax,
                filterStatus:status,
                "_token": "{{ csrf_token() }}"
            },
            // beforeSend: function () {
            //     $("#loading").show();
            // },
            success: function (data) {
                if(data.status == 1){
                  $('#table_report').empty();
                  $('#table_report').html(data.table_monitoring);
                  $("#table_report").DataTable( {
                              "bDestroy": true,
                              processing: true, //Feature control the processing indicator.
                              lengthChange: true,
                              pageLength: 10,
                              // scrollX: true,
                              buttons: [  {
                                              extend: "excelHtml5",
                                              title: "Report Validator"
                                          },
                                        ],
                              // "dom" : '<"row"<"col-sm-4"B><"col-sm-4"i><"col-sm-4"f>>rt<"row"<"col-sm-4"l><"col-sm-4"><"col-sm-4"p>>',
                              "dom" : "Bifrtlip",
                              "language": {
                                  "search": "Cari:",
                                  "emptyTable":     "Tidak ada data!",
                                  "info":           "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                                  "infoEmpty":      "Menampilkan 0 - 0 dari 0 data",
                                  "infoFiltered":   "(terfilterisasi dari _MAX_ jumlah data)",
                                  "thousands":      ".",
                                  "lengthMenu":     "Tampilkan _MENU_ data",
                                  "loadingRecords": "Loading...",   
                                  "processing": "Sedang proses.."                      
                              }
                  } );
                }else{
                  $('#table_report').empty();
                  alert(data.ket);
                }
                
            },
            complete: function(){
              $('html, body').animate({
                      scrollTop: $("#table_report").offset().top
                  }, 1000);
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
        }
  });


  </script>
@endpush
@endsection