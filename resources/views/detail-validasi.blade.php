@extends('layouts.main')
@section('container')
@push('validasi-style')
@endpush  
{{-- Template Detail Validasi --}}
<div class="row">
  <div class="col-lg-12 ">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb" style="background-color :#FFFFFF;">
        <li class="breadcrumb-item"><a href="/validasi">Validasi Data</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$idR}}</li>
      </ol>
    </nav>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <!-- Nama Order dan Status -->
        <div class="detail-header-validasi">
          <div class="row">
            <div class="col-lg-6 d-block text-center" >
              <h6><strong>ID Order :</strong></h6>
              <h6 id="idOrder" style=" font-family : Poppins; color :#EC8057; font-size :20px; font-weight:700;">{{$idR}}</h6>
            </div>
            <div class="col-lg-6 d-block text-center" >
              <h6><strong>Status : </strong></h6>
              <h6 style=" font-family : Poppins; color :#EC8057; font-size :20px; font-weight:700;">{{$statusValidasi}}</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row formvalidasi mt-4">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="col-lg-12 md-3">
          <H5 style="font-weight: bold;">Detail Data</H5>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <form>
              <fieldset disabled>
              <!-- Nama Pengirim -->
              <div class="form-group">
                <label for="disabledTextInput"><strong>Nama Pengirim</strong></label>
                <input type="text" id="disabledTextInput" class="form-control" placeholder="{{$namaKirim}}">
              </div>
              <!-- Alamat Pengirim -->
              <div class="form-group">
                <label for="exampleFormControlTextarea1"><strong>Alamat Pengirim</strong></label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="{{$alamatKirim}}"></textarea>
              </div>
              <!-- No HP Pengirim -->
              <div class="form-group">
                <label for="disabledTextInput"><strong>No. HP Pengirim</strong></label>
                <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="{{$hpPengirim}}">
              </div>
          </div>
          <div class="col-lg-6">
          <!-- Nama Penerima -->
          <fieldset disabled>
          <div class="form-group">
          <label for="disabledTextInput"><strong>Nama Penerima</strong></label>
          <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="{{$namaTerima}}">
          </div>
          <!-- Alamat Penerima -->
          <div class="form-group">
          <label for="exampleFormControlTextarea1"><strong>Alamat Penerima</strong></label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="{{$alamatTerima}}"></textarea>
          </div>
          <!-- No HP Penerima -->
          <div class="form-group">
          <label for="disabledTextInput"><strong>No. HP Pengirim</strong></label>
          <input type=textarea cols="30" rows="10" id="disabledTextInput" class="form-control" placeholder="{{$hpTerima}}">
          </div>
          </div>
          <div class="col-lg-12">
          <form>
          <hr>
          <fieldset>
          <div class="form-group">
          <label for="disabledTextInput"><strong>Layanan</strong></label>
          <div class="form-group" hidden>
          <select class="form-control" id="layanan_awal" >
          <option value="{{$layanan1}}" selected disabled>{{$layanan1}}</option>  
          <option value='POS Premium'>POS Premium</option>
          <option value='POS Sameday'>POS Sameday</option>
          <option value='POS Next day'>POS Next day</option>
          <option value='POS Reguler'>POS Reguler</option>
          <option value='PAKET KILAT KHUSUS'>PAKET KILAT KHUSUS</option>
          </select>
          </div>
          <div class="form-group">
          <select class="form-control" id="layanan" >
          <option value='POS Premium'>POS Premium</option>
          <option value='POS Sameday'>POS Sameday</option>
          <option value='POS Next day'>POS Next day</option>
          <option value='POS Reguler'>POS Reguler</option>
          <option value='PAKET KILAT KHUSUS'>PAKET KILAT KHUSUS</option>
          </select>
          </div>
          </div>
          <div class="form-group">
          <label for="disabledTextInput"><strong>Nilai Barang (Rp)</strong></label>
          <input type=textarea cols="30" rows="10" id="nilaibarang_awal" class="form-control" placeholder="Nilai Barang" value="150000" hidden>
          <input type=textarea cols="30" rows="10" id="nilaibarang" class="form-control" placeholder="Nilai Barang" value="150000">
          </div>
          <div class="row">
          <div class="col-lg-6">
          <div class="form-group">
          <label for="disabledTextInput"><strong>Kode Pos Asal</strong></label>
          <input type=textarea cols="30" rows="10" id="kodeposasal_awal" class="form-control" placeholder="Kode Pos Asal" value="{{$kodeAsal}}" hidden>
          <input type=textarea cols="30" rows="10" id="kodeposasal" class="form-control" placeholder="Kode Pos Asal" value="{{$kodeAsal}}">
          </div>
          </div>
          <div class="col-lg-6">
          <div class="form-group">
          <label for="disabledTextInput"><strong>Kode Pos Tujuan</strong></label>
          <input type=textarea cols="30" rows="10" id="kodepostujuan_awal" class="form-control" placeholder="Kode Pos Tujuan" value="{{$kodeTujuan}}" hidden>
          <input type=textarea cols="30" rows="10" id="kodepostujuan" class="form-control" placeholder="Kode Pos Tujuan" value="{{$kodeTujuan}}">
          </div>
          </div>
          </div>
          <div class="form-group">
          <label><strong>Berat Paket (Gram)</strong></label>
          <input type=textarea cols="30" rows="10" id="berat_awal" class="form-control"  placeholder="Berat Aktual" value="{{$berat}}" hidden>
          <input type=textarea cols="30" rows="10" id="berat" class="form-control" placeholder="Berat Aktual" value="{{$berat}}">
          </div>
          <div class="row">
          <div class="col-lg-12">
          <label for="disabledTextInput"><strong>Dimensi Paket P x L x T (CM)</strong></label>
          </div>
          </div>
          <div class="row">                             
          <div class="col-lg-4">
          <div class="form-group">
          <input type=number cols="30" rows="10" id="panjang_awal" class="form-control" placeholder="Panjang" value="{{$panjang}}" hidden>
          <input type=number cols="30" rows="10" id="panjang" class="form-control" placeholder="Panjang" value="{{$panjang}}">
          </div>
          </div>
          <div class="col-lg-4">
          <div class="form-group">
          <input type=number cols="30" rows="10" id="lebar_awal" class="form-control" placeholder="Lebar" value="{{$lebar}}" hidden>
          <input type=number cols="30" rows="10" id="lebar" class="form-control" placeholder="Lebar" value="{{$lebar}}">
          </div>
          </div>
          <div class="col-lg-4">
          <div class="form-group">
          <input type=number cols="30"  rows="10" id="tinggi_awal" class="form-control" placeholder="Tinggi" value="{{$tinggi}}" hidden>
          <input type=number cols="30"  rows="10" id="tinggi" class="form-control" placeholder="Tinggi" value="{{$tinggi}}">
          </div>
          </div>            
          </div>
          </fieldset>
          </form>
          </div>
        </div>
        </fieldset>
        </form>
        <div class="card" style="margin: 20px;">
          <div class="row mt-8"  style= "justify-content: center;">
            <div class="col-lg-3 mt-4">
              <label style="font-size: 9pt;">pastikan data sesuai sebelum validasi</label>
              <button type="button" id="btn-validasi" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#validasi"  style="background-color: #037c33; border-color:#037c33">Validasi</button>
              <button type="button" id="btn-cek-tarif" class="btn btn-primary btn-sm btn-block " data-toggle="modal" data-target="#g-va-baru" style="background-color: #EC8057; border-color: #EC8057" >Cek Tarif</button>
            </div>
          </div>
          <div class="row"  style= "justify-content: center; margin:20px;">
            <div class="row mt-2" style= "justify-content: center;">
              <small id="" data-toggle="modal" data-dismiss="modal" href="#batalOrder" class="text-muted"><span style="color:#ED2105; cursor:pointer; padding:8px 16px; border-radius:4px; border:1px solid red;">Batalkan Order?</span></small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Modal Selisih Tarif Generate VA-->
<div class="modal fade" id="g-va-baru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-isi">
          <div class="row" style="padding: 20pt;">
            <div class="col-lg-12 col-md-12 d-block text-center">
              <div class="row">
              </div>
            </div>
            <div class="col-lg-12 col-md-12 d-block text-center mt-3">
              <div class="row" style="justify-content: center;">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Terbayar</th>
                      <th scope="col">Kurang Bayar</th>
                      <th scope="col">Lebih Bayar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Rp.30.000</th>
                      <td>Rp.5.000</td>
                      <td>-</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-lg-12">
              <button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-dismiss="modal" href="#generate"  style="background-color: #EC8057; border-color:#EC8057">Generate VA Baru</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Selisih Tarif Generate VA --}}
<!-- Modal Validasi-->
<div class="modal fade" id="validasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          {{-- <img class="logo" src="bootstrap-fe/images/ceklis-green.png" alt="logo"> --}}
        </div>
        <h5><strong>Validasi Data</strong></h5>
        <p style="color:black; padding:20px;"> <strong> Apakah anda yakin untuk validasi data ini?</p>
        </strong>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="submit" id="finalVal" class="btn btn-outline-light col-5" style="background-color : #037c33; color: #ffffff " data-dismiss="modal" >Validasi data ini</button>
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057 ;color: #EC8057;" data-dismiss="modal" >Kembali</button>
      </div>
    </div>
  </div>
</div>
<!-- End-Modal Validasi-->
<!-- Modal Batal Order-->
<div class="modal fade" id="batalOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          <img class="logo" src="/bootstrap-fe/images/warning.png" alt="logo" style="width: 30%;"> 
        </div>
        <h5><strong>Batalkan Order?</strong></h5>
        <p><strong>Apakah anda yakin ingin membatalkan order ini?</strong></p>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #2AC543; color: #2AC543" data-dismiss="modal" >Kembali</button>
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; background-color: rgb(227, 53, 53);" data-dismiss="modal" >Batalkan Order</button>
      </div>
    </div>
  </div>
</div>
<!-- End-Modal Batal Order-->
<!-- Modal Refund -->
<div class="modal fade" id="refund" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="row" style="align-content: center">
          <div class="col-lg-12">
            <h4>Pilih Metode Refund:</h4>
          </div>
        </div>
        <div class="row">
          <div class="modal-footer d-block text-center">
            <button type="button" class="btn btn-primary col-12" style="border-color: #2AC543;" data-dismiss="modal" >Refund via VA</button>
          </div>
        </div>
        <div class="row">
          <div class="modal-footer d-block text-center">
            <button type="button" class="btn btn-primary col-12" style="border-color: #2AC543;" data-dismiss="modal" >Refund via Debet GIRO</button>
          </div>
        </div>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #2AC543; color: #2AC543" data-dismiss="modal" >kembali</button>
      </div>
    </div>
  </div>
</div>
<!-- End Refund-->
<!-- Modal Generate VA Baru-->
<div class="modal fade" id="generate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          <img class="logo" src="/bootstrap-fe/images/va-icon.png" alt="logo">
        </div>
        <div class="row d-block text-center"  style="padding: 10pt;">
          <strong>
            <h4>Generate VA Baru</h4>
          </strong>
          <p style="color:black; padding:20px;"> <strong> Nomor Virtual Account baru yang akan dikirimkan
            kepada customer dengan nominal transfer sesuai dengan selisih
            tarif dari hasil validasi
          </p>
          </strong>
          <div class="p-2 mb-2 text-dark" style="background-color: beige;">
            <h3><strong>342663453455344</strong></h3>
          </div>
        </div>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; color: #EC8057" data-dismiss="modal" >Batal</button>
        <form action="{{route('kirimNotif')}}" method="post">
          @csrf
          <button type="submit" name ="buttonKirim" class="btn btn-primary col-5" style="border-color: #EC8057; background-color: #EC8057"  value="kirim">Beritahu Pengirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End-Modal Generate VA Baru-->
<!-- Modal Simpan -->
<div class="modal fade" id="modalSimpan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          <img class="logo" src="/bootstrap-fe/images/ceklis-green.png" alt="logo">
        </div>
        <h4><strong>VA berhasil dikirim!</strong></h4>
        <p>Data telah tersimpan direport</p>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; color: #EC8057" data-dismiss="modal" >Tutup!</button>
      </div>
    </div>
  </div>
</div>
<!-- End- Modal Simpan -->
<!-- Modal Validasi -->
<div class="modal fade" id="modalValidasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          <img class="logo" src="/bootstrap-fe/images/ceklis-green.png" alt="logo">
        </div>
        <h4><strong>Data Berhasil Divalidasi!</strong></h4>
        <p>Data telah tersimpan direport</p>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; color: #EC8057" data-dismiss="modal" >Tutup!</button>
      </div>
    </div>
  </div>
</div>
<!-- End- Modal Validasi -->
<!-- Modal Gagal -->
<div class="modal fade" id="modalGagal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-block text-center">
        <div class="mb-3">
          <!-- <img class="logo" src="bootstrap-fe/images/ceklis-green.png" alt="logo"> -->
        </div>
        <h4><strong>Validasi Gagal!</strong></h4>
        <p>Data gagal tersimpan, harap validasi kembali.</p>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; color: #EC8057" data-dismiss="modal" >Tutup!</button>
      </div>
    </div>
  </div>
</div>
<!-- End- Modal Gagal -->
<!-- Modal History Pembayaran-->
<div class="modal fade" id="history" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-lg-12" style="padding:20pt;">
          <div class="card mb-1">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h4 style="font-weight: bold;">Transaksi #2</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <p style="color :black; font-size :9pt;">Tanggal : 1 Desember 2022</p>
                </div>
                <div class="col-lg-6">
                  <p style="color :black; font-size :9pt;">No Transaksi : 232320120</p>
                </div>
              </div>
              <p style="color :black; font-weight:bold;">Nominal : <span style="color: #EC8057">Rp 5.000</span> </p>
              <p style="color :black; font-weight:bold;">Virtual Account : <span style="color: #EC8057"> 234302304933443</span></p>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h4  style="font-weight: bold;">Transaksi #1</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <p style="color :black; font-size :9pt;">Tanggal : 1 Desember 2022</p>
                </div>
                <div class="col-lg-6">
                  <p style="color :black; font-size :9pt;">No Transaksi : 232320120</p>
                </div>
              </div>
              <p style="color :black; font-weight:bold;">Nominal : <span style="color: #EC8057">Rp35.000</span> </p>
              <p style="color :black; font-weight:bold;">Virtual Account : <span style="color: #EC8057"> 234302304933443</span></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer d-block text-center">
        <button type="button" class="btn btn-outline-light col-5" style="border-color: #EC8057; color: #EC8057" data-dismiss="modal" >Batal</button>
      </div>
    </div>
  </div>
</div>
<!-- End-Modal History Pembayaran-->
</div>
</div>
</div>
<script type="text/javascript">

    @if ($status==200)
      $(document).ready(function(){
        $('#modalSimpan').modal('show');
      })
    @elseif($status==0)
      null
    @else
    $(document).ready(function(){
        $('#modalGagal').modal('show');
      })
    @endif
    
  $('#finalVal').on('click',function(e){
      e.preventDefault();
      // const id_ext = "QOB55942986091302";
      const id_ext = $('#idOrder').text();
      const filterA = "1";
      const resi ="P22xxxxxxxx";
      
        $.ajax({
            url: "{{ route('validasi.finale') }}",
            type: "POST",
            dataType: "json", 
            data:{
                qob:id_ext,
                filter:filterA,
                idResi:resi,
                "_token": "{{ csrf_token() }}"
            },
            // beforeSend: function () {
            //     $("#loading").show();
            // },
            success: function (data) {
              $('#modalValidasi').modal('show');
              // console.log(data);
              // alert(data.success);
                
            },
            
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
        
  });
  $('#btn-cek-tarif').on('click',function(e){
      e.preventDefault();     
      $.ajax({
          url: "{{ route('validasi.tarif') }}",
          type: "POST",
          dataType: "json", 
          crossOrigin: true,
          data:{
            "filter":"1",
            "external_id":"QOB55942986091302",
            "kode_pos_asal":"40115",
            "kode_pos_tujuan":"40115",
            "berat":"250",
            "panjang":"0",
            "lebar":"0",
            "tinggi":"0",
            "diameter":"0",
            "htnb":"6577.65|0.0",
            "ppn":"72.35|0.0",
            "total_harga":"6650.0",
            "keterangan":"MAKANAN",
            "_token": "{{ csrf_token() }}"
          },
          success: function (data) {
            console.log(data);
            alert(data.success);
              
          }
          
          // error: function(xhr, status, error) {
          //     var err = eval("(" + xhr.responseText + ")");
          //     alert(err.Message);
          // }
      });
        
  });
   
    
  $(document).ready(function(){
    
    $("#btn-cek-tarif").hide();
    $("#btn-validasi").show();
  
    $("#layanan,#berat,#panjang,#lebar,#tinggi").change(function(){
      var layanan_awal = $('#layanan_awal').val();
      var layanan = $('#layanan').val();
  
      var panjang_awal= $('#panjang_awal').val();
      var panjang = $('#panjang').val();
  
      var lebar_awal = $('#lebar_awal').val();
      var lebar = $('#lebar').val();
  
      var tinggi_awal = $('#tinggi_awal').val();
      var tinggi = $('#tinggi').val();
  
      var berat_awal = $('#berat_awal').val();
      var berat = $('#berat').val();
      
       if(layanan_awal != $('#layanan').val() || berat_awal != $('#berat').val() || panjang_awal != $('#panjang').val() || lebar_awal != $('#lebar').val()|| tinggi_awal != $('#tinggi').val() ){
          if(layanan == null || layanan ==0 || layanan ==''){
              $("#btn-cek-tarif").hide();
              $("#btn-validasi").hide();
              alert("silahkan lengkapi data Layanan");
          }else if(berat == null || berat ==0 || berat ==''){
              $("#btn-cek-tarif").hide();
              $("#btn-validasi").hide();
              alert("silahkan lengkapi data berat");
          }else if(panjang == null || panjang ==0 || panjang ==''){
              $("#btn-cek-tarif").hide();
              $("#btn-validasi").hide();
              alert("silahkan lengkapi data panjang");
          }else if(tinggi == null || tinggi ==0 || tinggi ==''){
              $("#btn-cek-tarif").hide();
              $("#btn-validasi").hide();
              alert("silahkan lengkapi data tinggi");
          }else if(lebar == null || lebar ==0 || lebar ==''){
              $("#btn-cek-tarif").hide();
              $("#btn-validasi").hide();
              alert("silahkan lengkapi data lebar");
          }else{
              $("#btn-cek-tarif").show();
              $("#btn-validasi").hide();
          }
       }else{
          $("#btn-cek-tarif").hide();
          $("#btn-validasi").show();
       }
  
  
    });
    
    
  });
</script>
<!-- {{-- End-Template Detail Validasi --}}
  {{-- @push('detail-validasi-script')  -->
<!-- <script src="/bootstrap-fe/js/detail-validasi.js"></script>  -->
@endpush
@endsection