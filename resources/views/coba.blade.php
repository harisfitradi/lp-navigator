@extends('layouts.main')
@section('container')
@push('validasi-style')
<!-- Custom styles for this page -->
<link href="/bootstrap-fe/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/datetime/1.2.0/css/dataTables.dateTime.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/searchpanes/2.1.0/css/searchPanes.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/select/1.5.0/css/select.dataTables.min.css" rel="stylesheet">

<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.2.0/js/dataTables.dateTime.min.js"></script>
<script src="https://cdn.datatables.net/searchpanes/2.1.0/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.5.0/js/dataTables.select.min.js"></script>
@endpush
<h1 class="h3 mb-2 text-gray-800">Report</h1>
<div class="row justify-content-center">
  <div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      {{-- 
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data</h6>
      </div>
      --}}
      <div class="card-body">
        <!-- DatePicker Example -->
        <div class="row">
          
          <div class="col-lg-12">
            <table cellspacing="5" cellpadding="5" class="mb-4" class="table-responsive">
              <tbody>
                <tr>
                  <td>Start Date:</td>
                  <td>
                    <input type="text" id="min" name="min" class="form-control" placeholder="Pilih Tanggal Awal">
                  </td>
                  <td>Status: </td>
                  <td>
                    <select class="form-select form-control" aria-label="Status" id="statFilter" name="statusFilter">
                      <option value="Semua Status">Semua Status</option>
                      <option value="Valid">Valid</option>
                      <option value="Menunggu Pembayaran">Menunggu Pembayaran</option>
                      <option value="Menunggu Validasi">Menunggu Validasi</option>
                      <option value="Cancel Order">Cancel Order</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>End Date:</td>
                  <td>
                    <input type="text" id="max" name="max" class="form-control" placeholder="Pilih Tanggal Akhir">
                  </td>
                  <td>
                    <!--  -->
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered" id="tabelData" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>ID Order</th>
                <th>Pengirim</th>
                <th>Kode Pos Asal</th>
                <th>Kode Pos Tujuan</th>
                <th>Layanan</th>
                <th>Berat Paket (gr)</th>
                <th>Biaya Kirim</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="tableBody">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script>

jQuery.ajax(

  {
    url : 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order',
    method: 'POST',
    crossDomain : true,
    type : "json",
    headers: 
    {
      "content-type":"application/json",
      "X-POS-USER":"xxx",
      "X-POS-PASSWORD":"yyy",
      
    },
    data: 
    {
      "filter":"1",
      "periode_awal":"2022-06-01",
      "periode_akhir":"2022-12-01",
      "external_id":"",
      "nopend":"",
      "status":""
    },
    success: function(response){
      var a = response;
      console.log(a);
    }

  });
  var dataArray = [
      {
          'tanggal':'2022-11-22',
          'id': 'QOB000001',
          'pengirim':'Komang Eufamia Sholeha',
          'kodeAsal':'40291',
          'kodeTujuan':'40291',
          'layanan':'Pos Instant',
          'berat':'3000',
          'biaya':'30000',
          'status':'Valid'
      },
      {
          'tanggal':'2022-11-23',
          'id': 'QOB000002',
          'pengirim':'M Haris Fitradi',
          'kodeAsal':'40291',
          'kodeTujuan':'40291',
          'layanan':'Pos Instant',
          'berat':'4000',
          'biaya':'40000',
          'status':'Menunggu Pembayaran'
      },
      {
          'tanggal':'2022-11-24',
          'id': 'QOB000003',
          'pengirim':'Farid Wajdi K',
          'kodeAsal':'40292',
          'kodeTujuan':'40293',
          'layanan':'Pos Instant',
          'berat':'5000',
          'biaya':'50000',
          'status':'Menunggu Validasi'
      },
      {
          'tanggal':'2022-11-25',
          'id': 'QOB000004',
          'pengirim':'Ruben Jupandi',
          'kodeAsal':'40293',
          'kodeTujuan':'40294',
          'layanan':'Pos Instant',
          'berat':'1000',
          'biaya':'10000',
          'status':'Cancel Order'
      },
      {
          'tanggal':'2022-11-26',
          'id': 'QOB000005',
          'pengirim':'Jesse Pinkman',
          'kodeAsal':'40293',
          'kodeTujuan':'40294',
          'layanan':'Pos Instant',
          'berat':'1000',
          'biaya':'10000',
          'status':'Valid'
      },
      {
          'tanggal':'2022-11-27',
          'id': 'QOB000006',
          'pengirim':'Dave Jules',
          'kodeAsal':'40293',
          'kodeTujuan':'40294',
          'layanan':'Pos Instant',
          'berat':'1000',
          'biaya':'10000',
          'status':'Valid'
      }
  ];
  createTable(dataArray);
  function createTable(data){
      var table = document.getElementById("tableBody");
      for(var i=0;i < data.length; i++){
          var row =  `<tr>
                          <td>${data[i].tanggal}</td>
                          <td><a href="/detail-report">${data[i].id}</a></td>
                          <td>${data[i].pengirim}</td>
                          <td>${data[i].kodeAsal}</td>
                          <td>${data[i].kodeTujuan}</td>
                          <td>${data[i].layanan}</td>
                          <td>${data[i].berat}</td>
                          <td>${data[i].biaya}</td>
                          <td>${data[i].status}</td>
                      </tr>`
          table.innerHTML += row
      }
  };
  
  var minDate, maxDate;
  // Custom filtering function which will search data in column 0 (tanggal)
  $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      var min = minDate.val();
      var max = maxDate.val();
      var date = new Date(data[0]);
      if (
          ( min === null && max === null ) ||
          ( min === null && date <= max ) ||
          ( min <= date && max === null ) ||
          ( min <= date && date <= max )
      ) {
          return true;
      }
      return false;
    }
  );
  
  $(document).ready(function() {

    // Create date inputs
    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    }); 
    // DataTables initialisation
    var newtable = $('#tabelData').DataTable();
    // Status Filter
    $("#statFilter").change(function(){
      var status=$("#statFilter").val();
      regExsearch= '^'+ status +'$';
      console.log(regExsearch);
      if(status==="Semua Status"){
        newtable.column(8).search('').draw();
      }
      else{
        newtable.column(8).search(regExsearch, true, false).draw();
      }
      
    });

    // Refilter date on the table
    $('#min, #max').on('change', function () {
        newtable.draw();
    });
  });
  
</script>
@push('validasi-script')
<!-- <script src="/bootstrap-fe/datatables/jquery.dataTables.min.js"></script> -->
<!-- <script src="/bootstrap-fe/datatables/dataTables.bootstrap4.min.js"></script> -->
<script src="/bootstrap-fe/js/custom-datatables.js"></script>
@endpush
@endsection