<?php


use App\Http\Controllers\LoginController;
use App\Http\Controllers\endpointController;
use App\Http\Controllers\ValidasiController;
use App\Models\post;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// LOGIN
Route::get('/', [LoginController::class, 'login'])->name('login')->middleware('guestValidator');
// LANDING/IMPORT
Route::post('/import',[LoginController::class, 'authenticate'])->name('authenticate');
Route::get('/import', function() {
    return view ('import', [
        "title" => 'Import',
        'status' => '0'
    ]);})->name('import')->middleware('authValidator');
// REPORT
Route::get('/report', function() {
    return view ('report', [
        "title" => 'Report',
        'status' => '0'
    ]);})->name('report')->middleware('authValidator');
// DETAIL VALIDASI
// Route::get('/detail-validasi', function() {
//     return view ('detail-validasi', [
//         "title" => 'Detail Validasi',
//         'status' => '0'
//     ]);})->name('detail-validasi')->middleware('authValidator');
    Route::get('/detail-validasi/{id}', [endpointController::class, 'detailValidasi'])->name('detail-validasi')->middleware('authValidator');
// DETAIL REPORT
// Route::get('/detail-report/{id}', function() {
//     return view ('detail-report', [
//         "title" => 'Detail Report',
//         'status' => '0',
//         // 'id' => '{id}'
//     ]);})->name('detail-report')->middleware('authValidator');
Route::get('/detail-report/{id}', [ValidasiController::class, 'detailReport'])->name('detail-report')->middleware('authValidator');

// RESI
Route::get('/resi', function() {
    return view ('cetakresi', [
        "title" => 'Cetak Resi',
        'status' => '0'
    ]);})->name('cetakresi')->middleware('authValidator');
// LOGOUT
Route::get('/logout', [LoginController::class, 'logout'])->name('logout')->middleware('authValidator');
// KIRIM NOTIF
Route::post('/detail-validasi', [endpointController::class, 'kirimNotif'])->name('kirimNotif')->middleware('authValidator');
// Route::post('/detail-validasi', function() {
//     return view ('detail-validasi', [
//         "title" => 'Send Notif',
//         'status' => '0'
//     ]);})->name('kirimNotif')->middleware('authValidator');

// Route::get('/coba', [LoginController::class, 'handle'])->name('coba');
// Route::post('/coba', [LoginController::class, 'handle'])->name('coba');// VALIDASI
Route::get('/validasi', [ValidasiController::class, 'index'])->name('validasi')->middleware('authValidator');
// GET DATA
// Route::post('/validasi', [ValidasiController::class, 'store'])->name('getData')->middleware('authValidator');
Route::post('/post-monitoring', [ValidasiController::class, 'postMonitoring'])->name('post.monitoring')->middleware('authValidator');
Route::post('/post-report', [endpointController::class, 'postReport'])->name('post.report')->middleware('authValidator');
Route::post('/validasi-finale', [endpointController::class, 'validasiFinal'])->name('validasi.finale')->middleware('authValidator');
Route::post('/get-status-import', [ValidasiController::class, 'getStatusImport'])->name('get.status.import')->middleware('authValidator');
Route::post('/get-status-import-pickuper', [ValidasiController::class, 'getStatusImportPickuper'])->name('get.status.import.pickuper')->middleware('authValidator');
Route::post('/post-monitoring-qr', [ValidasiController::class, 'postMonitoringQR'])->name('post.monitoring.qr')->middleware('authValidator');
Route::post('/post-monitoring-pickuper', [ValidasiController::class, 'postMonitoringPickuper'])->name('post.monitoring.pickuper')->middleware('authValidator');
Route::post('/validasi-tarif', [endpointController::class, 'validasiTarif'])->name('validasi.tarif')->middleware('authValidator');
Route::post('/terima-barang', [ValidasiController::class, 'terimaBarang'])->name('terima.barang')->middleware('authValidator');
