<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class endpointController extends Controller
{
    // KIRIM NOTIF
    public function kirimNotif(request $request){
        $cek = $request->input('buttonKirim');
        // dd($cek);
        $apiURL = 'https://order.posindonesia.co.id/api_dev/notification/bidding';
        $email = 'bayuadijaya@gmail.com';
        $title = 'Informasi Kurang Bayar Pengiriman PosAja';
        $qob = 'QOB55942986091302';
        $selisih = '5000';
        $va = '342663453455344';
        $hashing = md5($email.'1tb4ngkurl06'.$title);
        // pos data
        $postInput = [
            'email'=>$email,
            'title'=>$title,
            'message'=>'Kiriman Anda dengan nomor '.$qob.' terdapat kekurangan bayar ongkos kirim sebesar Rp '.$selisih.'. Harap melunasi kurang bayar melalui nomor VA berikut: '.$va,
            'hashing'=>$hashing,
            'phone'=>'',
            'no_pickup'=>'',
            'status'=>'a1'
        ];
        // headers
        $headers = [
            'Content-type'=>'application/json',
            'Authorization'=>'Basic cXBvc2luZG9hbmc6UDA1QjRHVXMxOQ==', 
            'x-api-key'=>'p1CkUP2625'
        // ..
        ];

        $response = Http::withHeaders($headers)->post($apiURL,  $postInput);
        $responseBody = json_decode($response->getBody(), true);
        $statusCode = $response->status();
        if($statusCode==200){
            return redirect()->back()->with('status',200);
        }
        else{
            return redirect()->back()->with('status',10);
        }
        // dd($statusCode);
        // $s_statusCode = Session::put('statusCode',$statusCodeA);
        // return redirect('/detail-validasi')->with('username', Session::get('username'),'jabatan', Session::get('jabatan'),'status', Session::get('statusCode'));
        // return view('/detail-validasi',['username' => Session::get('username'),'jabatan' => Session::get('jabatan'),'status'=>$statusCode]);
        // dd($statusCode);
    }

    public function postReport(Request $request)
    {
        if(session('username')){
            if(substr(session('idpetugas'),0,3) == '820'){
                $tglAwal = $request->tglAwal;
                $tglAkhir = $request->tglAkhir;
                $filterStatus = $request->filterStatus;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order';
                if(!empty($filterStatus)){
                    $params = '{
                        "filter":"3",
                        "periode_awal":"'.$tglAwal.'",
                        "periode_akhir":"'.$tglAkhir.'",
                        "external_id":"",
                        "nopend":"'.session('kdkantor').'",
                        "status":"'.$filterStatus.'"
                    }';
                }else{
                    $params = '{
                        "filter":"1",
                        "periode_awal":"'.$tglAwal.'",
                        "periode_akhir":"'.$tglAkhir.'",
                        "external_id":"",
                        "nopend":"'.session('kdkantor').'",
                        "status":""
                    }';
                }

                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_report = curl_exec($ch);
    
                if ($post_report === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode(['status' => 0,
                                        'ket' => $get_error_curl
                                        ]);
                } else {
                    $result = json_decode($post_report, true);
                    $html='';
                    if(count($result['response']['data']) != 0){
                        if(isset($result['response']['data']['keterangan'])){
                            $data = $result['response']['data'];
                            $no = 1;
                            $html.='
                                <table id="list-report">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                ';

                                $keterangan = $data['keterangan'];
                                $layanan = $data['layanan'];
                                $tlp_penerima = $data['tlp_penerima'];
                                $tlp_pengirim = $data['tlp_pengirim'];
                                $full_asal = $data['full_asal'];
                                $full_tujuan = $data['full_tujuan'];
                                $nama_pengirim = $data['nama_pengirim'];
                                $nama_penerima = $data['nama_penerima'];
                                $kodepos_asal = $data['kodepos_asal'];
                                $kodepos_tujuan = $data['kodepos_tujuan'];
                                $ext_id = $data['ext_id'];
                                $total_harga = $data['total'];
                                $alias_berat = $data['alias_berat'];
                                $panjang = $data['panjang'];
                                $lebar = $data['lebar'];
                                $tinggi = $data['tinggi'];
                                $diameter = $data['diameter'];
                                $driver = $data['driver'];
                                $kprk_asal = $data['kprk_asal'];
                                $status = $data['status'];
                                $insert_date = $data['insert_date'];
                                $update_date = $data['update_date'];
                                $routeID = '/detail-report'.'/'.$ext_id;
                                
                                $html.='
                                    <tr>
                                        <td>'.$no.'</td>
                                        <td><a href="'.$routeID.'">'.$ext_id.'</a></td>
                                        <td>'.strtoupper($layanan).'</td>
                                        <td>'.ucwords($nama_pengirim).'<br>('.$tlp_pengirim.')</td>
                                        <td>'.ucwords($nama_penerima).'<br>('.$tlp_penerima.')</td>
                                        <td>'.$full_asal.'<br>'.$kodepos_asal.'</td>
                                        <td>'.$full_tujuan.'<br>'.$kodepos_tujuan.'</td>
                                        <td>PxLxT:'.$panjang.'x'.$lebar.'x'.$tinggi.'<br>Diameter:'.$diameter.'<br>Berat:'.$alias_berat.'</td>
                                        <td>Rp. '.number_format($total_harga,0,'.',',').'</td>
                                        <td>'.ucwords($driver).'</td>
                                        <td>'.date('Y-m-d H:i:s',strtotime($insert_date)).'</td>
                                        <td>'.date('Y-m-d H:i:s',strtotime($update_date)).'</td>
                                        <td>'.$status.'</td>
                                    </tr>         
                                    
                                ';
                            $html.='</tbody>
                            </table>';
                        }else{
                            $all_data = $result['response']['data'];

                            $no = 1;
                            $html.='
                                <table id="list-report">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                ';

                            foreach($all_data as $data){
                                $keterangan = $data['keterangan'];
                                $layanan = $data['layanan'];
                                $tlp_penerima = $data['tlp_penerima'];
                                $tlp_pengirim = $data['tlp_pengirim'];
                                $full_asal = $data['full_asal'];
                                $full_tujuan = $data['full_tujuan'];
                                $nama_pengirim = $data['nama_pengirim'];
                                $nama_penerima = $data['nama_penerima'];
                                $kodepos_asal = $data['kodepos_asal'];
                                $kodepos_tujuan = $data['kodepos_tujuan'];
                                $ext_id = $data['ext_id'];
                                $total_harga = $data['total'];
                                $alias_berat = $data['alias_berat'];
                                $panjang = $data['panjang'];
                                $lebar = $data['lebar'];
                                $tinggi = $data['tinggi'];
                                $diameter = $data['diameter'];
                                $driver = $data['driver'];
                                $kprk_asal = $data['kprk_asal'];
                                $status = $data['status'];
                                $insert_date = $data['insert_date'];
                                $update_date = $data['update_date'];
                                $routeID = '/detail-report'.'/'.$ext_id;
                                
                                $html.='
                                    <tr>
                                        <td>'.$no.'</td>
                                        <td\><a href="'.$routeID.'">'.$ext_id.'</a></td>
                                        <td>'.strtoupper($layanan).'</td>
                                        <td>'.ucwords($nama_pengirim).'<br>('.$tlp_pengirim.')</td>
                                        <td>'.ucwords($nama_penerima).'<br>('.$tlp_penerima.')</td>
                                        <td>'.$full_asal.'<br>'.$kodepos_asal.'</td>
                                        <td>'.$full_tujuan.'<br>'.$kodepos_tujuan.'</td>
                                        <td>PxLxT:'.$panjang.'x'.$lebar.'x'.$tinggi.'<br>Diameter:'.$diameter.'<br>Berat:'.$alias_berat.'</td>
                                        <td>Rp. '.number_format($total_harga,0,'.',',').'</td>
                                        <td>'.ucwords($driver).'</td>
                                        <td>'.date('Y-m-d H:i:s',strtotime($insert_date)).'</td>
                                        <td>'.date('Y-m-d H:i:s',strtotime($update_date)).'</td>
                                        <td>'.$status.'</td>
                                        
                                    </tr>         
                                    
                                ';
                            $no++;
                            }
                            $html.='</tbody>
                            </table>';
                        }

                        return json_encode(['status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'table_report' => $html
                        ]);
                    }else{
                        return json_encode(['status' => 0,
                                            'ket' => 'Tidak ada data!'
                                            ]);
                    }
    
                }
                //close connection
                curl_close($ch);

            }else{
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function detailReport($id){
        $idReport = $id;
        $tanggal = "22 November 2022";
        $namaKirim="Komang Eufamia";
        $alamatKirim="Kosan Rumah Kita, Kamar C2.13, Jl. Antapani, no. 50 (samping SMPN 49), RT01, RW05, Kel. Antapani Kulon, Kec. Antapani, Bandung, 40291";
        $kodeAsal="40291";
        $hpPengirim="08123456789";
        $namaTerima="Made Immanuel Bayu";
        $alamatTerima="Kosan Rumah Kita, Kamar C2.13, Jl. Antapani, no. 50 (samping SMPN 49), RT01, RW05, Kel. Antapani Kulon, Kec. Antapani, Bandung, 40291";
        $kodeTujuan="40291";
        $hpTerima="08123456789";
        $layanan1="POS Instant";
        $berat="3000";
        $nominal="150000";
        $tarifBayar="30000";
        $dimensi="10"." x "."10"." x "."10";
        $nominalVoucher="0";
        $status="Valid";
        return view('detail-report',
        [
            'idR'=>$idReport, 
            'tanggal'=>$tanggal,
            'namaKirim'=>$namaKirim,
            'alamatKirim'=>$alamatKirim,
            'kodeAsal'=>$kodeAsal,
            'hpPengirim'=>$hpPengirim,
            'namaTerima'=>$namaTerima,
            'alamatTerima'=>$alamatTerima,
            'kodeTujuan'=>$kodeTujuan,
            'hpTerima'=>$hpTerima,
            'layanan1'=>$layanan1,
            'berat'=>$berat,
            'nominal'=>$nominal,
            'tarifBayar'=>$tarifBayar,
            'dimensi'=>$dimensi,
            'nominalVoucher'=>$nominalVoucher,   
            'status'=>$status
        ]);
    }
    
    public function detailValidasi($id){
        $idV = $id;
        if(session('username')){
            if(substr(session('idpetugas'),0,3) == '820'){
                $qob = $id;
                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order';
                
                $params = 
                '{
                    "filter":"2",
                    "periode_awal":"",
                    "periode_akhir":"",
                    "external_id":"'.$qob.'",
                    "nopend":"",
                    "status":""
                }';
                
                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $get_validasi = curl_exec($ch);
    
                if ($get_validasi === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode(['status' => 0,
                                        'ket' => $get_error_curl
                                        ]);
                } else {
                    $result = json_decode($get_validasi, true);
                    $html='';
                    if(count($result['response']['data']) != 0){
                        if(isset($result['response']['data']['keterangan'])){
                            $data = $result['response']['data'];
                           
                            $keterangan = $data['keterangan'];
                            $layanan = $data['layanan'];
                            $tlp_penerima = $data['tlp_penerima'];
                            $tlp_pengirim = $data['tlp_pengirim'];
                            $full_asal = $data['full_asal'];
                            $full_tujuan = $data['full_tujuan'];
                            $nama_pengirim = $data['nama_pengirim'];
                            $nama_penerima = $data['nama_penerima'];
                            $kodepos_asal = $data['kodepos_asal'];
                            $kodepos_tujuan = $data['kodepos_tujuan'];
                            $ext_id = $data['ext_id'];
                            $total_harga = $data['total'];
                            $alias_berat = $data['alias_berat'];
                            $panjang = $data['panjang'];
                            $lebar = $data['lebar'];
                            $tinggi = $data['tinggi'];
                            $diameter = $data['diameter'];
                            $driver = $data['driver'];
                            $kprk_asal = $data['kprk_asal'];
                            $statusValidasi = $data['status'];
                            $insert_date = $data['insert_date'];
                            $update_date = $data['update_date'];
                            $voucher = $data['nilaivoucher'];
                                                        
                            return view('detail-validasi',[
                                'idR'=>$ext_id, 
                                'tanggal'=>$insert_date,
                                'namaKirim'=>$nama_pengirim,
                                'alamatKirim'=>$full_asal,
                                'kodeAsal'=>$kodepos_asal,
                                'hpPengirim'=>$tlp_pengirim,
                                'namaTerima'=>$nama_penerima,
                                'alamatTerima'=>$full_tujuan,
                                'kodeTujuan'=>$kodepos_tujuan,
                                'hpTerima'=>$tlp_penerima,
                                'layanan1'=>$layanan,
                                'berat'=>$alias_berat,
                                // 'nominal'=>$nominal,
                                'tarifBayar'=>$total_harga,
                                'panjang'=>$panjang,
                                'lebar'=>$lebar,
                                'tinggi'=>$tinggi,
                                'nominalVoucher'=>$voucher,   
                                'statusValidasi'=>$statusValidasi,
                                'status'=>'0'
                            ]);
                                
                        }else{
                            $data = $result['response']['data'];

                            $keterangan = $data['keterangan'];
                            $layanan = $data['layanan'];
                            $tlp_penerima = $data['tlp_penerima'];
                            $tlp_pengirim = $data['tlp_pengirim'];
                            $full_asal = $data['full_asal'];
                            $full_tujuan = $data['full_tujuan'];
                            $nama_pengirim = $data['nama_pengirim'];
                            $nama_penerima = $data['nama_penerima'];
                            $kodepos_asal = $data['kodepos_asal'];
                            $kodepos_tujuan = $data['kodepos_tujuan'];
                            $ext_id = $data['ext_id'];
                            $total_harga = $data['total'];
                            $alias_berat = $data['alias_berat'];
                            $panjang = $data['panjang'];
                            $lebar = $data['lebar'];
                            $tinggi = $data['tinggi'];
                            $diameter = $data['diameter'];
                            $driver = $data['driver'];
                            $kprk_asal = $data['kprk_asal'];
                            $statusValidasi = $data['status'];
                            $insert_date = $data['insert_date'];
                            $update_date = $data['update_date'];
                            $voucher = $data['nilaivoucher'];
                                                       
                            return view('detail-validasi',[
                                'idR'=>$ext_id, 
                                'tanggal'=>$insert_date,
                                'namaKirim'=>$nama_pengirim,
                                'alamatKirim'=>$full_asal,
                                'kodeAsal'=>$kodepos_asal,
                                'hpPengirim'=>$tlp_pengirim,
                                'namaTerima'=>$nama_penerima,
                                'alamatTerima'=>$full_tujuan,
                                'kodeTujuan'=>$kodepos_tujuan,
                                'hpTerima'=>$tlp_penerima,
                                'layanan1'=>$layanan,
                                'berat'=>$alias_berat,
                                // 'nominal'=>$nominal,
                                'tarifBayar'=>$total_harga,
                                'panjang'=>$panjang,
                                'lebar'=>$lebar,
                                'tinggi'=>$tinggi,
                                'nominalVoucher'=>$voucher,   
                                'statusValidasi'=>$statusValidasi,
                                'status'=>'0'
                            ]);
                        }

                        return json_encode(['status' => 1,
                            'ket' => 'Berhasil mendapatkan data!'
                            
                        ]);
                    }else{
                        return json_encode(['status' => 0,
                                            'ket' => 'Tidak ada data!'
                                            ]);
                    }
    
                }
                //close connection
                curl_close($ch);

            }else{
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
            
        }else{
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
        $status = '0';
        return view('detail-validasi',
        [
            'id'=>$id,
            'status'=>$status
        ]);
    }
    
    // public function validasiFinal(request $request){
    //     $ddd= $request->all();
    //     // dd($ddd);
    //     return response()->json(['success'=>'Got Simple Ajax Request.']);
    // }

    public function validasiFinal(request $request){
        if(session('username')){
            if(substr(session('idpetugas'),0,3) == '820'){
                $ext_id = $request->qob;
                $filter = "1";
                $id_resi = $request->idResi;
                // dd($request);
             
                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/validasi_final_validator';

                $params = '{
                    "filter":"'.$filter.'",
                    "external_id":"'.$ext_id.'",
                    "id_resi":"'.$id_resi.'"
                }';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $response = curl_exec($ch);
                $err = curl_error($ch);
                $result = json_decode($response, true);
                
                curl_close($ch);
                if ($err) {
                // dd($err);
                } else {
                // echo $response;
                // dd($result);
                return $result;
                }
            }
            else{
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
            
        }
        else{
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function validasiTarif(){
        if(session('username')){
            if(substr(session('idpetugas'),0,3) == '820'){
                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/validasi_tarif_validator';        
                $params = 
                '{
                    "filter": "1",
                    "external_id": "QOB55942986091302",
                    "kode_pos_asal": "40115",
                    "kode_pos_tujuan": "40115",
                    "berat": "250",
                    "panjang": "0",
                    "lebar": "0",
                    "tinggi": "0",
                    "diameter": "0",
                    "htnb": "6577.65|0.0",
                    "ppn": "72.35|0.0",
                    "total_harga": "6650.0",
                    "keterangan": "MAKANAN"
                  }';
                
                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $get_tarif = curl_exec($ch);
    
                if ($get_tarif === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode(['status' => 0,
                                        'ket' => $get_error_curl
                                        ]);
                } else {
                    $result = json_decode($get_tarif, true);
                    $html='';
                    if(count($result['response']['data']) != 0){
                        if(isset($result['response']['data']['keterangan'])){
                            $data = $result['response']['data'];
                            dd($data);                                                     
                                                            
                        }else{
                            $data = $result['response']['data'];
                            dd($data);
                            
                        }

                        return json_encode(['status' => 1,
                            'ket' => 'Berhasil mendapatkan data!'
                            
                        ]);
                    }else{
                        return json_encode(['status' => 0,
                                            'ket' => 'Tidak ada data!'
                                            ]);
                    }
    
                }
                //close connection
                curl_close($ch);

            }else{
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
            
        }else{
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
        $status = '0';
        return view('detail-validasi',
        [
            'id'=>$id,
            'status'=>$status
        ]);
    }
}
