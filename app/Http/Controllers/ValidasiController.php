<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class ValidasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('validasi', []);
    }

    // controller import barang
    public function imprtBrg(Request $request)
    {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                $tglAwal = $request->tglAwal;
                $tglAkhir = $request->tglAkhir;
                $filterStatus = $request->filterStatus;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order';
                if (!empty($filterStatus)) {
                    $params = '{
                        "filter":"3",
                        "periode_awal":"' . $tglAwal . '",
                        "periode_akhir":"' . $tglAkhir . '",
                        "external_id":"",
                        "nopend":"' . session('kdkantor') . '",
                        "status":"' . $filterStatus . '"
                    }';
                } else {
                    $params = '{
                        "filter":"1",
                        "periode_awal":"' . $tglAwal . '",
                        "periode_akhir":"' . $tglAkhir . '",
                        "external_id":"",
                        "nopend":"' . session('kdkantor') . '",
                        "status":""
                    }';

                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);

                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';
                    if (count($result['response']['data']) != 0) {
                        if (isset($result['response']['data']['keterangan'])) {
                            $data = $result['response']['data'];
                            $no = 1;
                            $html .= '
                                <table id="list-monitoring">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                    <tbody>
                            ';

                            $keterangan = $data['keterangan'];
                            $layanan = $data['layanan'];
                            $tlp_penerima = $data['tlp_penerima'];
                            $tlp_pengirim = $data['tlp_pengirim'];
                            $full_asal = $data['full_asal'];
                            $full_tujuan = $data['full_tujuan'];
                            $nama_pengirim = $data['nama_pengirim'];
                            $nama_penerima = $data['nama_penerima'];
                            $kodepos_asal = $data['kodepos_asal'];
                            $kodepos_tujuan = $data['kodepos_tujuan'];
                            $ext_id = $data['ext_id'];
                            $total_harga = $data['total'];
                            $alias_berat = $data['alias_berat'];
                            $panjang = $data['panjang'];
                            $lebar = $data['lebar'];
                            $tinggi = $data['tinggi'];
                            $diameter = $data['diameter'];
                            $driver = $data['driver'];
                            $kprk_asal = $data['kprk_asal'];
                            $status = $data['status'];
                            $insert_date = $data['insert_date'];
                            $update_date = $data['update_date'];
                            $routeID = '/detail-validasi'.'/'.$ext_id;

                            $html .= '
                                <tr>
                                    <td>' . $no . '</td>
                                    <td><a href="'.$routeID.'">'.$ext_id.'</a></td>
                                    <td>' . strtoupper($layanan) . '</td>
                                    <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                    <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                    <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                    <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                    <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                    <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                    <td>' . ucwords($driver) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>       
                                    <td>'.$status.'</td>                            
                                </tr>         
                                
                            ';
                            $html .= '</tbody>
                            </table>';
                        } else {
                            $all_data = $result['response']['data'];

                            $no = 1;
                            $html .= '
                                <table id="list-monitoring">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                ';

                            foreach ($all_data as $data) {
                                $keterangan = $data['keterangan'];
                                $layanan = $data['layanan'];
                                $tlp_penerima = $data['tlp_penerima'];
                                $tlp_pengirim = $data['tlp_pengirim'];
                                $full_asal = $data['full_asal'];
                                $full_tujuan = $data['full_tujuan'];
                                $nama_pengirim = $data['nama_pengirim'];
                                $nama_penerima = $data['nama_penerima'];
                                $kodepos_asal = $data['kodepos_asal'];
                                $kodepos_tujuan = $data['kodepos_tujuan'];
                                $ext_id = $data['ext_id'];
                                $total_harga = $data['total'];
                                $alias_berat = $data['alias_berat'];
                                $panjang = $data['panjang'];
                                $lebar = $data['lebar'];
                                $tinggi = $data['tinggi'];
                                $diameter = $data['diameter'];
                                $driver = $data['driver'];
                                $kprk_asal = $data['kprk_asal'];
                                $status = $data['status'];
                                $insert_date = $data['insert_date'];
                                $update_date = $data['update_date'];
                                $routeID = '/detail-validasi'.'/'.$ext_id;

                                $html .= '
                                    <tr>
                                        <td>' . $no . '</td>
                                        <td><a href="'.$routeID.'">'.$ext_id.'</a></td>
                                        <td>' . strtoupper($layanan) . '</td>
                                        <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                        <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                        <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                        <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                        <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                        <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                        <td>' . ucwords($driver) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>   
                                        <td>'.$status.'</td>    
                                    </tr>
                                ';
                                $no++;
                            }
                            $html .= '</tbody>
                            </table>';
                        }

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'qob' => $ext_id,
                            'table_monitoring' => $html
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'ket' => 'Tidak ada data!'
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function postMonitoringQR(Request $request)
    {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                
                $QOB = $request->idBarang;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order';
                
                
                    $params = '{
                        "filter":"2",
                        "periode_awal":"",
                        "periode_akhir":"",
                        "external_id":"'.$QOB.'",
                        "nopend":"",
                        "status":""
                    }';

                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);

                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';
                    if (count($result['response']['data']) != 0) {
                        if (isset($result['response']['data']['keterangan'])) {
                            $data = $result['response']['data'];
                            $no = 1;
                            $html .= '
                                <table id="list-monitoring-2" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Aksi</th>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                    </tr>
                                </thead>
                                    <tbody>
                            ';

                            $keterangan = $data['keterangan'];
                            $layanan = $data['layanan'];
                            $tlp_penerima = $data['tlp_penerima'];
                            $tlp_pengirim = $data['tlp_pengirim'];
                            $full_asal = $data['full_asal'];
                            $full_tujuan = $data['full_tujuan'];
                            $nama_pengirim = $data['nama_pengirim'];
                            $nama_penerima = $data['nama_penerima'];
                            $kodepos_asal = $data['kodepos_asal'];
                            $kodepos_tujuan = $data['kodepos_tujuan'];
                            $ext_id = $data['ext_id'];
                            $total_harga = $data['total'];
                            $alias_berat = $data['alias_berat'];
                            $panjang = $data['panjang'];
                            $lebar = $data['lebar'];
                            $tinggi = $data['tinggi'];
                            $diameter = $data['diameter'];
                            $driver = $data['driver'];
                            $kprk_asal = $data['kprk_asal'];
                            $status = $data['status'];
                            $insert_date = $data['insert_date'];
                            $update_date = $data['update_date'];

                            $html .= '
                                <tr>
                                    <td><button class="btn btn-info detailButton" type="button" data-extid="'.$ext_id.'">Terima Barang</button></td>                                                                            
                                    <td>' . $no . '</td>
                                    <td class="qob">' . $ext_id . '</td>
                                    <td>' . strtoupper($layanan) . '</td>
                                    <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                    <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                    <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                    <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                    <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                    <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                    <td>' . ucwords($driver) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>
                                </tr>         
                                
                            ';
                            $html .= '</tbody>
                            </table>';
                        } else {
                            $all_data = $result['response']['data'];

                            $no = 1;
                            $html .= '
                                <table id="list-monitoring-2">
                                <thead>
                                    <tr>
                                        <th>Aksi</th>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                ';

                            foreach ($all_data as $data) {
                                $keterangan = $data['keterangan'];
                                $layanan = $data['layanan'];
                                $tlp_penerima = $data['tlp_penerima'];
                                $tlp_pengirim = $data['tlp_pengirim'];
                                $full_asal = $data['full_asal'];
                                $full_tujuan = $data['full_tujuan'];
                                $nama_pengirim = $data['nama_pengirim'];
                                $nama_penerima = $data['nama_penerima'];
                                $kodepos_asal = $data['kodepos_asal'];
                                $kodepos_tujuan = $data['kodepos_tujuan'];
                                $ext_id = $data['ext_id'];
                                $total_harga = $data['total'];
                                $alias_berat = $data['alias_berat'];
                                $panjang = $data['panjang'];
                                $lebar = $data['lebar'];
                                $tinggi = $data['tinggi'];
                                $diameter = $data['diameter'];
                                $driver = $data['driver'];
                                $kprk_asal = $data['kprk_asal'];
                                $status = $data['status'];
                                $insert_date = $data['insert_date'];
                                $update_date = $data['update_date'];

                                $html .= '
                                    <tr>
                                        <td><button class="btn btn-info detailButton" type="button" data-extid="'.$ext_id.'">Terima Barang</button></td>                                        
                                        <td>' . $no . '</td>
                                        <td class="qob">' . $ext_id . '</td>
                                        <td>' . strtoupper($layanan) . '</td>
                                        <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                        <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                        <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                        <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                        <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                        <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                        <td>' . ucwords($driver) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>
                                    </tr>
                                ';
                                $no++;
                            }
                            $html .= '</tbody>
                            </table>';
                        }

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'qob' => $ext_id,
                            'table_monitoring' => $html
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'ket' => 'Tidak ada data!'
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function postMonitoringPickuper(Request $request)
    {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                $tanggal = $request->tanggal;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/monitoring_validator_order';
                
                    $params = '{
                        "filter":"3",
                        "periode_awal":"' . $tanggal . '",
                        "periode_akhir":"' . $tanggal . '",
                        "external_id":"",
                        "nopend":"",
                        "status":"2"
                    }';

                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);

                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';
                    if (count($result['response']['data']) != 0) {
                        if (isset($result['response']['data']['keterangan'])) {
                            $data = $result['response']['data'];
                            $no = 1;
                            $html .= '
                                <table id="list-monitoring-1" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Aksi</th>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                    </tr>
                                </thead>
                                    <tbody>
                            ';

                            $keterangan = $data['keterangan'];
                            $layanan = $data['layanan'];
                            $tlp_penerima = $data['tlp_penerima'];
                            $tlp_pengirim = $data['tlp_pengirim'];
                            $full_asal = $data['full_asal'];
                            $full_tujuan = $data['full_tujuan'];
                            $nama_pengirim = $data['nama_pengirim'];
                            $nama_penerima = $data['nama_penerima'];
                            $kodepos_asal = $data['kodepos_asal'];
                            $kodepos_tujuan = $data['kodepos_tujuan'];
                            $ext_id = $data['ext_id'];
                            $total_harga = $data['total'];
                            $alias_berat = $data['alias_berat'];
                            $panjang = $data['panjang'];
                            $lebar = $data['lebar'];
                            $tinggi = $data['tinggi'];
                            $diameter = $data['diameter'];
                            $driver = $data['driver'];
                            $kprk_asal = $data['kprk_asal'];
                            $status = $data['status'];
                            $insert_date = $data['insert_date'];
                            $update_date = $data['update_date'];

                            $html .= '
                                <tr>
                                    <td><button class="btn btn-info detailButton" type="button" data-extid="'.$ext_id.'">Terima Barang</button></td>                                 
                                    <td>' . $no . '</td>
                                    <td class="qob">' . $ext_id . '</td>
                                    <td>' . strtoupper($layanan) . '</td>
                                    <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                    <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                    <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                    <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                    <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                    <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                    <td>' . ucwords($driver) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                    <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>
                                </tr>         
                                
                            ';
                            $html .= '</tbody>
                            </table>';
                        } else {
                            $all_data = $result['response']['data'];
                            $no = 1;
                            $html .= '
                                <table id="list-monitoring-1" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Aksi</th>
                                        <th>No.</th>
                                        <th>External ID</th>
                                        <th>Layanan</th>
                                        <th>Nama Pengirim</th>
                                        <th>Nama Penerima</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Volume Barang/<br>Berat</th>
                                        <th>Total Harga</th>
                                        <th>Driver</th>
                                        <th>Tanggal Insert</th>
                                        <th>Tanggal Update</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                ';

                            foreach ($all_data as $data) {
                                $keterangan = $data['keterangan'];
                                $layanan = $data['layanan'];
                                $tlp_penerima = $data['tlp_penerima'];
                                $tlp_pengirim = $data['tlp_pengirim'];
                                $full_asal = $data['full_asal'];
                                $full_tujuan = $data['full_tujuan'];
                                $nama_pengirim = $data['nama_pengirim'];
                                $nama_penerima = $data['nama_penerima'];
                                $kodepos_asal = $data['kodepos_asal'];
                                $kodepos_tujuan = $data['kodepos_tujuan'];
                                $ext_id = $data['ext_id'];
                                $total_harga = $data['total'];
                                $alias_berat = $data['alias_berat'];
                                $panjang = $data['panjang'];
                                $lebar = $data['lebar'];
                                $tinggi = $data['tinggi'];
                                $diameter = $data['diameter'];
                                $driver = $data['driver'];
                                $kprk_asal = $data['kprk_asal'];
                                $status = $data['status'];
                                $insert_date = $data['insert_date'];
                                $update_date = $data['update_date'];

                                $html .= '
                                    <tr>
                                        <td><button class="btn btn-info detailButton" type="button" data-extid="'.$ext_id.'">Terima Barang</button></td>
                                        <td>' . $no . '</td>
                                        <td class="qob">' . $ext_id . '</td>
                                        <td>' . strtoupper($layanan) . '</td>
                                        <td>' . ucwords($nama_pengirim) . '<br>(' . $tlp_pengirim . ')</td>
                                        <td>' . ucwords($nama_penerima) . '<br>(' . $tlp_penerima . ')</td>
                                        <td>' . $full_asal . '<br>' . $kodepos_asal . '</td>
                                        <td>' . $full_tujuan . '<br>' . $kodepos_tujuan . '</td>
                                        <td>PxLxT:' . $panjang . 'x' . $lebar . 'x' . $tinggi . '<br>Diameter:' . $diameter . '<br>Berat:' . $alias_berat . '</td>
                                        <td>Rp. ' . number_format($total_harga, 0, '.', ',') . '</td>
                                        <td>' . ucwords($driver) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($insert_date)) . '</td>
                                        <td>' . date('Y-m-d H:i:s', strtotime($update_date)) . '</td>  
                                    </tr>
                                ';
                                $no++;
                            }
                            $html .= '</tbody>
                            </table>';
                        }

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'qob' => $ext_id,
                            'table_monitoring' => $html
                        ]);
                    } else {
                        return json_encode([
                            'status' => 0,
                            'ket' => 'Tidak ada data!'
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function getStatusImport(Request $request) {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                $idBarang = $request->idBarang;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/insert_validator_order';
                
                $params = 
                    '{
                        "filter":"1",
                        "external_id": "'.$idBarang.'",
                        "id_user":"",
                        "create_date":""
                    }';
                
                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);
                //dd($post_monitoring);
                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';

                    if($result['response']['data']["respmsg"] == 'Sukses insert ID Order' && $result['response']['data']["respmsg"] == 000) {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'response' => $html
                        ]);
                    } else {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';
                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data walaupun kosong!',
                            'response' => $html
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function getStatusImportPickuper(Request $request) {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                $tanggal = $request->tanggal;
                $oranger = $request->oranger;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/insert_validator_order';
                
                $params = 
                    '{
                        "filter":"2",
                        "external_id": "",
                        "id_user":"'.$oranger.'",
                        "create_date":"'.$tanggal.'"
                    }';
                
                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);
                //dd($post_monitoring);
                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';

                    if($result['response']['data']["respmsg"] == 'Sukses insert ID Order' && $result['response']['data']["respmsg"] == 000) {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'response' => $html
                        ]);
                    } else {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';
                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data walaupun kosong!',
                            'response' => $html
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }

    public function terimaBarang(Request $request) {
        if (session('username')) {
            if (substr(session('idpetugas'), 0, 3) == '820') {
                $idBarang = $request->idBarang;

                $url = 'http://10.29.41.40:8280/qposinajadev/1.0.0/insert_validator_order';
                
                $params = 
                    '{
                        "filter":"3",
                        "external_id": "'.$idBarang.'",
                        "id_user":"",
                        "create_date":""
                    }';
                
                //open connection
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'X-POS-USER: xxx',
                    'X-POS-PASSWORD: yyy'
                ));
                $post_monitoring = curl_exec($ch);
                //dd($post_monitoring);
                if ($post_monitoring === false) {
                    $get_error_curl = 'Curl Error: ' . curl_error($ch);
                    return json_encode([
                        'status' => 0,
                        'ket' => $get_error_curl
                    ]);
                } else {
                    $result = json_decode($post_monitoring, true);
                    $html = '';

                    if($result['response']['data']["respmsg"] == 'Sukses update status ID Order' && $result['response']['data']["respmsg"] == 000) {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';

                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data!',
                            'response' => $html
                        ]);
                    } else {
                        $html .= '
                        <p>'.$result['response']['data']['respmsg'].'</p>
                        <p>'.$result['response']['data']['respcode'].'</p>
                        ';
                        return json_encode([
                            'status' => 1,
                            'ket' => 'Berhasil mendapatkan data walaupun kosong!',
                            'response' => $result['response']['data']['respmsg']
                        ]);
                    }
                }
                //close connection
                curl_close($ch);
            } else {
                return json_encode(['status' => 0, 'ket' => 'Maaf, anda tidak dapat mengakses menu ini!']);
            }
        } else {
            return json_encode(['status' => 0, 'ket' => 'Maaf, anda diharuskan login!']);
        }
    }
}
