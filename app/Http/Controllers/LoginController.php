<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
// use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function authenticate (Request $request)
    {
        $apiURL = 'https://jembatan.posindonesia.co.id/iposutility/dev/1.0.0/login';
        
        $postInput = [
            'idpetugas'=>$request->input('idpetugas'),
            'password'=>$request->input('password'),
            'imei'=> '',
            'version'=>'1.0.0',
            'frontend'=>'pickupmobile',
            'deviceid'=> ''
        ];
        
        $headers = [

        ];

        $response = Http::withHeaders($headers)->post($apiURL,  $postInput);

        $statusCode = $response->status();
        $responseBody = json_decode($response->getBody(), true);

        $username = $responseBody['response']['username'];
        $idpetugas = $responseBody['response']['idpetugas'];
        $jabatan = $responseBody['response']['jabatan'];
        $jabatanmitra = $responseBody['response']['jabatanmitra'];
        $kdkantor = $responseBody['response']['kdkantor'];
        $kantor = $responseBody['response']['kantor'];

        if($username !='-'){
            $s_username         = Session::put('username', $username);
            $s_idpetugas        = Session::put('idpetugas', $idpetugas);
            $s_jabatan          = Session::put('jabatan', $jabatan);
            $s_jabatan_mitra    = Session::put('jabatanmitra', $jabatanmitra);
            $s_kdkantor         = Session::put('kdkantor', $kdkantor);
            $s_kantor           = Session::put('kantor', $kantor);
            
            return redirect('import')->with('success-login', 'Berhasil Login !');
        }else{
            return back()->with('error-login', 'Username/Password Salah');
        }

    }
    
    public function logout(){
        Session::flush();
        return redirect('/')->with('success-logout', 'Berhasil Logout');
    }  

}
