<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class validator extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'validator';
    protected $fillable = [

        'tanggal_order',
        'id_order',
        'pengirim',
        'penerima',
        'no_penerima',
        'no_pengirim',
        'layanan',
        'berat',
        'kodepos_asal',
        'kodepos_tujuan',
        'biaya',
        'voucher',
        'status'

    ];

    protected $hidden = [];
}
